const express = require("express")
const app = express()
const mysql = require("mysql")
const bcrypt = require("bcrypt")
require("dotenv").config()
const crypto = require("crypto")
const cors = require('cors');
const jwt = require("jsonwebtoken")
const tokenChecker = require('../dbServer/tokenChecker')


const DB_HOST = process.env.DB_HOST
const DB_USER = process.env.DB_USER
const DB_PASSWORD = process.env.DB_PASSWORD
const DB_DATABASE = process.env.DB_DATABASE
const DB_PORT = process.env.DB_PORT

const port = process.env.PORT || 5001;

const generateAccessToken = require("../dbServer/generateAccessToken")
const generateRefreshToken = require("../dbServer/generateRefreshToken")
const { getuid } = require("process")



const db = mysql.createPool({
    connectionLimit: 100,
    host: DB_HOST,
    user: DB_USER,
    password: DB_PASSWORD,
    database: DB_DATABASE,
    port: DB_PORT
 })

 app.use(express.json())
 app.use(cors())
//  app.use(require('./tokenChecker'))

 app.post("/createUser", async (req, res) => {

    const salt = bcrypt.genSalt(10);
    const username = req.body.username;
    const hashedPassword = await bcrypt.hash(req.body.password, 10)
    const firstName = req.body.firstName
    const lastName = req.body.lastName
    const email = req.body.email
    const birthDate = req.body.birthDate
    const birthDateF = birthDate.slice(0, 10)

    db.getConnection( async (err, connection) => {

        if (err) throw (err)

        const sqlSearch = "SELECT * FROM User WHERE username = ?";
        const search_query = mysql.format(sqlSearch,[username]);

        const sqlInsert = "INSERT INTO User VALUES (0, ?, ?, ?, ?, ?, ?)";
        const insert_query = mysql.format(sqlInsert, [firstName, lastName, username, email, hashedPassword, birthDateF]);
        
        await connection.query(search_query, async (err,result) => {
            if (err) throw (err)
            console.log("---> Search results");
            console.log(result.length)

            if (result.length != 0) {
                connection.release()
                console.log("---> User already exists");
                res.sendStatus(404);
            }
            else {
                await connection.query(insert_query, (err, result) => {
                    connection.release
                    if (err) throw (err)
                    console.log( "-----> Created new user");
                    console.log(result.insertID)
                    res.sendStatus(201)
                })
            }
        })
        
    })
})

app.post("/login", async(req, res) => {

    const user = req.body.username;
    const password = req.body.password


    db.getConnection( async (err, connection) => {
        if (err) throw (err)

        const usernameSearch = "SELECT * FROM User WHERE username = ?";
        const usernamesearch_query = mysql.format(usernameSearch,[user]);

        await connection.query(usernamesearch_query, async (err,result) => {
            connection.release()

            if (err) throw (err)
            console.log("---> Search results");
            console.log(result.length)

            if (result.length == 0) {
                console.log("---> User does not exist");
                res.json({accessToken: undefined})
            }
            else {
                if (await bcrypt.compare(password, result[0].password)) {
                    console.log("---> User successfully logged in")
                    console.log("---------> Generating accessToken")
                    const accessToken = generateAccessToken({user: user})   
                    const refreshToken = generateRefreshToken({user: user})   
                    res.json({accessToken: accessToken, refreshToken: refreshToken})
                }
                else {
                    console.log("---> User did not successfully log in")
                    res.json({accessToken: undefined})
                }
            }
        })
    })
})

app.get("/getFriends", tokenChecker, async(req, res) => {
    const username = req.query.username;

    var myFriends = []

    // const token = req.query.token.slice(1, -1)
    // console.log(token)
    // if (token) {
    //     const decode = jwt.verify(token, process.env.ACCESS_TOKEN_SECRET)
    //     console.log(decode)
    // }
            db.getConnection( async(err, connection) => {
                if (err) throw (err)
        
                const friendSearch =  "SELECT u1.*\
                                    FROM User u1\
                                    INNER JOIN Friendship f ON u1.userID = f.receiverID\
                                    INNER JOIN User u2 ON f.requesterID = u2.userID\
                                    WHERE u2.username = ?\
                                    UNION\
                                    SELECT u1.*\
                                    FROM User u1\
                                    INNER JOIN Friendship f ON u1.userID = f.requesterID\
                                    INNER JOIN User u2 ON f.receiverID = u2.userID\
                                    WHERE u2.username = ?"
        
                const friend_query = mysql.format(friendSearch, [username, username])
        
                await connection.query(friend_query, async (err, result) => {
        
                    if (err) {console.log(err)
                        throw (err)
                        }
        
                    if (result.length === 0) {
                        console.log("---> User does not exist");
                        res.send([])
                    }
                    else {
                        console.log("---> Friends list returned")
                        res.json(result)
                    }
                })
        
            })
        })


app.get("/getCurrentUser", tokenChecker, async(req, res) => {
    const username = req.query.username;
    
    db.getConnection( async(err, connection) => {
        if (err) throw (err)

        const friendSearch =  "SELECT *\
                            FROM User\
                            WHERE username = ?"

        const friend_query = mysql.format(friendSearch, [username])

        await connection.query(friend_query, async (err, result) => {

            if (err) {console.log(err)
                throw (err)
                }

            if (result.length === 0) {
                console.log("---> User does not exist");
                res.sendStatus(404)
            }
            else {
                console.log("user info sent")
                res.json(result)
            }
        })

        
    })
})

app.post("/addFriend", tokenChecker, async(req, res) => {
    const requester = req.body.requester;
    const receiver = req.body.receiver;
    // console.log(requester)
    // console.log(receiver)
    // var friendToAdd, currentUser


    
    // if (requesterID && receiverID) {
        db.getConnection( async(err, connection) => {

            if (err) throw (err)

            const friendSearch =  "SELECT *\
                                FROM User\
                                WHERE username = ?"

            const friend_query = mysql.format(friendSearch, [receiver])

            await connection.query(friend_query, async (err, result) => {
                if (requester === receiver) {
                    res.send("You cannot add yourself as a friend")
                    return
                }
                
                if (err) {console.log(err)
                    throw (err)
                    }
                if (result.length === 0) {
                    console.log("---> User does not exist");
                    res.send("User does not exist")
                }
                else {
                    console.log("user info sent")
                    const friend_query_two = mysql.format(friendSearch, [requester])
                    let friendToAdd = result
                    await connection.query(friend_query_two, async (err, result) => {

                        if (err) {console.log(err)
                            throw (err)
                            }
        
                        if (result.length === 0) {
                            console.log("---> User does not exist");
                            res.send("User does not exist")
                        }
                        else {
                            console.log("user info sent")
                            let currentUser = result
                            if (err) throw (err)
    
                            var date = new Date()
                    
                            date = date.toISOString().slice(0, 19).replace('T', ' ')
                    
                            let requesterID = currentUser[0]['userID']
                            let receiverID = friendToAdd[0]['userID']

                            console.log(requesterID)
                            console.log(receiverID)

                            const addFriend = "INSERT INTO Friendship VALUES (?, ?, ?)"
                            const addFriend_query = mysql.format(addFriend, [requesterID, receiverID, date])
                    
                            const friendshipSearch = "SELECT * FROM Friendship WHERE requesterID = ? AND receiverID = ? UNION SELECT * FROM Friendship WHERE receiverID = ? AND requesterID = ?";

                            const friendshipSearch_query = mysql.format(friendshipSearch,[requesterID, receiverID, requesterID, receiverID]);

                            await connection.query(friendshipSearch_query, async (err,result) => {
                                if (err) throw (err)
                                console.log("---> Search results friendship");
                                console.log(result.length)
                    
                                if (result.length != 0) {
                                    connection.release()
                                    console.log("---> Friendship already exists");
                                    res.send("You are already friends with this user")
                                }
                                else if (result.length === 0) {
                                    await connection.query(addFriend_query, async (err, result) => {
                                        if (err) throw (err)
                    
                                        connection.release()
                                        console.log()
                                        res.send("Friend added successfully")
                                    })
                                }
                            })
                        }
                    })
                }
            })
        })
    })

app.get("/getPreviousGames", tokenChecker, async(req, res) => {
    const username = req.query.username;
    console.log(username)
    
    db.getConnection( async(err, connection) => {
        if (err) throw (err)

        const gameSearch =    "SELECT *\
                                FROM GAMES\
                                WHERE loser = ?\
                                UNION\
                                SELECT *\
                                FROM GAMES\
                                WHERE winner = ?\
                                "

        const game_query = mysql.format(gameSearch, [username, username])

        await connection.query(game_query, async (err, result) => {

            if (err) throw (err)

            connection.release()

            if (result.length === 0) {
                console.log("---> No games");
                res.sendStatus(404)
            }
            else {
                console.log("previous game info sent")
                res.json(result)
            }
        })

        
    })
})

app.get("/getGameStates", tokenChecker, async(req, res) => {
    const gameID = req.query.gameID;
    
    db.getConnection( async(err, connection) => {
        if (err) throw (err)

        const prevStatesSearch =    "SELECT *\
                                FROM PREVIOUSSTATES\
                                WHERE gameID = ?\
                                "

        const states_query = mysql.format(prevStatesSearch, [gameID])

        await connection.query(states_query, async (err, result) => {

            if (err) throw (err)

            connection.release()

            if (result.length === 0) {
                console.log("---> No previous states for this game");
                res.sendStatus(404)
            }
            else {
                console.log("previous states sent")
                res.json(result)
            }
        })

        
    })
})

app.get("/getWLD", tokenChecker, async(req, res) => {
    const username = req.query.username;
    var wins, losses, draws
    var jsonResponse = {
        "wins":0,
        "losses":0,
        "draws":0
    }
    
    db.getConnection( async(err, connection) => {
        if (err) throw (err)

        const winSearch =    "SELECT COUNT(winner) FROM GAMES WHERE winner=? AND draw=0"
        const losSearch =    "SELECT COUNT(winner) FROM Games WHERE loser=? AND draw=0"

        const drawSearch =    "SELECT COUNT(winner) FROM Games WHERE loser=? AND draw=1 OR winner=? AND draw=1"

        const winSearch_query = mysql.format(winSearch, [username])
        const losSearch_query = mysql.format(losSearch, [username])
        const drawSearch_query = mysql.format(drawSearch, [username, username])

        connection.query(winSearch_query, async (err, result) => {

            if (err) throw (err)

            if (result.length === 0) {
                console.log("---> No wins");
                res.sendStatus(404)
                jsonResponse['wins'] = 0
            }
            else {
                jsonResponse['wins'] = result
                wins = result
                console.log(jsonResponse)
            }
        })

        connection.query(losSearch_query, async (err, result) => {

            if (err) throw (err)

            if (result.length === 0) {
                console.log("---> No wins");
                res.sendStatus(404)
                jsonResponse['losses'] = 0
            }
            else {
                jsonResponse['losses'] = result
                losses = result
            }
        })

        connection.query(drawSearch_query, async (err, result) => {

            if (err) throw (err)

            connection.release()

            if (result.length === 0) {
                console.log("---> No wins");
                res.sendStatus(404)
                jsonResponse['draws'] = 0
            }
            else {
                jsonResponse['draws'] = result
                
            }
            res.json(jsonResponse)
        })

        

        
    })
})



 app.listen(port, 
    ()=> console.log(`Server Started on port ${port}...`))