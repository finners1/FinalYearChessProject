const jwt = require("jsonwebtoken")
const mysql = require("mysql")

const DB_HOST = process.env.DB_HOST
const DB_USER = process.env.DB_USER
const DB_PASSWORD = process.env.DB_PASSWORD
const DB_DATABASE = process.env.DB_DATABASE
const DB_PORT = process.env.DB_PORT

const db = mysql.createPool({
    connectionLimit: 100,
    host: DB_HOST,
    user: DB_USER,
    password: DB_PASSWORD,
    database: DB_DATABASE,
    port: DB_PORT
 })


let refreshTokens = []

function generateRefreshToken(user) {
    const refreshToken = 
    jwt.sign(user, process.env.REFRESH_TOKEN_SECRET, {expiresIn: "20m"})

    //ADDS REFRESH KEYS TO DATABASE - NOT SURE IF THIS IS NECESSARY
    //
    // db.getConnection( async (err, connection) => {
    //     if (err) throw (err)

    //     const insertSQL = "INSERT INTO refreshTokens VALUES (0, ?)"
    //     const insert_query = mysql.format(insertSQL,[refreshToken]);

    //     await connection.query(insert_query, async (err,result) => {
    //         if (err) throw (err)
    //         console.log(err)
    //     })
    // })

    refreshTokens.push(refreshToken)
    return refreshToken
}

module.exports=generateRefreshToken
