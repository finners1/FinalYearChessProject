const jwt = require('jsonwebtoken')

module.exports = (req,res,next) => {
  const tokenWithQuotes = req.body.token || req.query.token || req.headers['x-access-token']
  // decode token
  const token = tokenWithQuotes.slice(1, -1)
  if (token) {
    // verifies secret and checks exp
    jwt.verify(token, process.env.ACCESS_TOKEN_SECRET, function(err, decoded) {
        if (err) {
            return res.status(401).json({"error": true, "message": 'Unauthorized access.' });
        }
      req.decoded = decoded;
      next();
    });
  } else {
    return res.status(403).send({
        "error": true,
        "message": 'No token provided.'
    });
  }
}