import RegistrationForm from './components/Registration/registrationForm'
import LoginForm from './components/Login/loginForm'
import { BrowserRouter as Router, Navigate, Route, Routes } from 'react-router-dom';
import PrivateRoute from './components/PrivateRoute/privateRoute';
import Board from './components/Board/Board';
import Home from './components/Home/home'
import AddFriend from './components/AddFriend/addFriend'
import Profile from './components/Profile/profile'
import 'bootstrap/dist/css/bootstrap.min.css';
import { socket, SocketContext } from "./components/Context/socket";
import React, {useState,useEffect} from 'react';
import socketIOClient from 'socket.io-client';
import history from './history';
const ENDPOINT = "http://127.0.0.1:6001";

function App() {

  const [isGameStarted, setIsGameStarted] = useState(false)
  const [roomID, setRoomID] = useState('')
  const [gameData, setGameData] = useState({})
  const [colour, setColour] = useState('')
  const [isGameSelected, setIsGameSelected] = useState(false)
  const [boardHistory, setBoardHistory] = useState([])
  const [viewMoveHistory, setViewMoveHistory] = useState([])

  const gameStartConfirmation = (data) => {
    // If select opponent player then start game and redirect to game play
    // this.setState({ isGameStarted: data.status, gameId: data.game_id, gameData: data.game_data });
    setIsGameStarted(data.status);
    setRoomID(data.roomID)
    setGameData(data.gameData)
  };

  const viewGameConfirmation = (data, colour, moves) => {
    console.log(data)
    setIsGameSelected(true)
    setBoardHistory(data)
    setColour(colour)
    setViewMoveHistory(moves)
  }

  const opponentLeft = (data) => {
    // If opponent left then get back from game play to player screen
    alert("Opponent Left");
    setIsGameStarted(false);
    setRoomID(null)
    setGameData(null)
  };

  const colourAssignment = (data) => {
    setColour(data)
  }

  const leaveViewGame = (data) => {
    setIsGameSelected(false)
  }

  // const [token, setToken] = useState();

  // if (!token) {
  //   return <Login setToken={setToken} />
  // }
  // const [response, setResponse] = useState("");

  // useEffect(() => {
  //   const socket = socketIOClient(ENDPOINT);
  //   socket.on("FromAPI", data => {
  //     setResponse(data)
  //   })
  // }, []);
  // const username = 1;

  // useEffect(() => {
  //   const socket = socketIOClient(ENDPOINT);
  //     socket.on('connect', function() {
  //       socket.emit('connected', {username: username})
  //     })
  // }, []);
  

    
  return (
    <div id="app">
      <h1 className="header">Chess</h1>
      {/* <p>
      It's <time dateTime={response}>{response}</time>
      </p> */}
      {/* <Router>
          <Routes>
            <Route path="/" element = {<LoginForm />} />
            <Route path="/register" element = {<RegistrationForm />} />
            <Route path="/login" element = {<LoginForm />} />
          </Routes>
      </Router> */}
      <SocketContext.Provider value={socket}>
        <Router>
          <Routes>
            <Route path="/" element = {<LoginForm />} />
            <Route path="/register" element = {<RegistrationForm />} />
            <Route path="/login" element = {<LoginForm />} />
            
            <Route path="/home" element= {
              
              <PrivateRoute>
                <Home gameStartConfirmation={gameStartConfirmation} colourAssignment={colourAssignment} isGameStarted={isGameStarted}/>
              </PrivateRoute> 
              }
            />
            <Route path="/game" element= {
             
              <PrivateRoute>
              {isGameStarted ?
                <Board roomID = {roomID} gameData = {gameData} colour ={colour} opponentLeft={opponentLeft} gameStartConfirmation={gameStartConfirmation}/>
                 : 
                <Home gameStartConfirmation={gameStartConfirmation} colourAssignment={colourAssignment}/>
              }
              </PrivateRoute> 
              }
            />
            <Route path="/profile" element= {
              
              <PrivateRoute>
              {isGameSelected ?
                <Board boardHistory = {boardHistory} colour={colour} viewMoveHistory={viewMoveHistory}/> :
                <Profile viewGameConfirmation={viewGameConfirmation} leaveViewGame={leaveViewGame}/>
              }
                
              </PrivateRoute> 
             }
            />
            <Route path="/addFriends" element= {
              <PrivateRoute>
                <AddFriend></AddFriend>
              </PrivateRoute> }
            />
           
          </Routes>
        </Router>
        </SocketContext.Provider>
      
      
    </div>
  );
}

export default App;

