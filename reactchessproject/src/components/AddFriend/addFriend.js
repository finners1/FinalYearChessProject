import React, {useEffect, useState} from 'react';
import '../Registration/register.css'
import axios from 'axios';
import { useNavigate } from "react-router-dom";

function AddFriend() {

    const [friendUsername, setFriendUsername] = useState("");
    const [friendToAdd, setFriendToAdd] = useState(null)
    const [currentUser, setCurrentUser] = useState(null)
    let navigate = useNavigate(); 
    const username = localStorage.getItem("username");
    const accessToken = localStorage.getItem("accessToken")

    const handleInputChange = (e) => {
        const {id , value} = e.target;
        if(id === "friendUsername"){
            setFriendUsername(value);
        }
    }

    const handleSubmit  = async() => {
            const addFriendObject = {
                        "requester": username,
                        "receiver": friendUsername,
                        "token" : accessToken
                    };
                    // axios.post('https://db-server-final.herokuapp.com/addFriend', addFriendObject)
                    axios.post('http://localhost:5001/addFriend', addFriendObject)
                      .then((res) => {
                        window.alert(res.data)
                    }).catch((error) => {
                        console.log(error)
                        // window.alert('You are already friends with this user')
                    });
        }


    return(
        <div className="form">
            <div className="form-body">
                <div className="username">
                    <label className="form__label" htmlFor="friendUsername">Enter Username of Friend </label>
                    <input  type="text" name="" id="friendUsername" value={friendUsername} onChange = {(e) => handleInputChange(e)} placeholder="Friend Username"/>
                </div>
            </div>
            <div className="footer">
                <button onClick={()=>handleSubmit()} type="submit" className="btn">Add friend</button>
            </div>
            {/* <Board /> */}
        </div>
       
    )       
}

export default AddFriend