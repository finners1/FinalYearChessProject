import React, { useContext, useEffect, useRef, useState } from 'react';
import Square from "../Square/Square";
import Timer from "../Timer/timer"


import io from "socket.io-client"
import './Board.css'
import socketIOClient from 'socket.io-client';
import { SocketContext } from "../Context/socket";
import { Navigate, useNavigate } from 'react-router-dom';
import { Row, Col, Container, Card, Button } from 'react-bootstrap'
import TakenPieces from '../TakenPieces/takenPieces';

// let endpoint = "http://localhost:5000"
// let socket = io.connect(`${endpoint}`);



const horizontalAxis = ["A", "B", "C", "D", "E", "F", "G", "H"]
const verticalAxis = ["1", "2", "3", "4", "5", "6", "7", "8"];


const startBoardState = [];
    var blackPawns = 1, whitePawns = 6, blackPieces = 0, whitePieces = 7
for (let i = 0; i < 8; i++) {
    startBoardState.push({image: "assets/img/blackPawn.png", x:i, y:6, pieceType : 1, teamType: 'black'});
}

for (let i = 0; i < 8; i++) {
    startBoardState.push({image: "assets/img/whitePawn.png", x:i, y:1, pieceType : 1, teamType: 'white'});
}

startBoardState.push({image: "assets/img/whiteRook.png", x:0, y:0, pieceType : 5, teamType: 'white'});
startBoardState.push({image: "assets/img/whiteKnight.png", x:1, y:0, pieceType : 2, teamType: 'white'});
startBoardState.push({image: "assets/img/whiteBishop.png", x:2, y:0, pieceType : 3, teamType: 'white'});
startBoardState.push({image: "assets/img/whiteQueen.png", x:3, y:0, pieceType : 9, teamType: 'white'});
startBoardState.push({image: "assets/img/whiteKing.png", x:4, y:0, pieceType : 10, teamType: 'white'});
startBoardState.push({image: "assets/img/whiteBishop.png", x:5, y:0, pieceType : 3, teamType: 'white'});
startBoardState.push({image: "assets/img/whiteKnight.png", x:6, y:0, pieceType : 2, teamType: 'white'});
startBoardState.push({image: "assets/img/whiteRook.png", x:7, y:0, pieceType : 5, teamType: 'white'});

startBoardState.push({image: "assets/img/blackRook.png", x:0, y:7, pieceType : 5, teamType: 'black'});
startBoardState.push({image: "assets/img/blackKnight.png", x:1, y:7, pieceType : 2, teamType: 'black'});
startBoardState.push({image: "assets/img/blackBishop.png", x:2, y:7, pieceType : 3, teamType: 'black'});
startBoardState.push({image: "assets/img/blackQueen.png", x:3, y:7, pieceType : 9, teamType: 'black'});
startBoardState.push({image: "assets/img/blackKing.png", x:4, y:7, pieceType : 10, teamType: 'black'});
startBoardState.push({image: "assets/img/blackBishop.png", x:5, y:7, pieceType : 3, teamType: 'black'});
startBoardState.push({image: "assets/img/blackKnight.png", x:6, y:7, pieceType : 2, teamType: 'black'});
startBoardState.push({image: "assets/img/blackRook.png", x:7, y:7, pieceType : 5, teamType: 'black'});
// function initialiseBoard() {

// }

//{roomID, gameData} : {roomID : any, gameData: any}
    

export default function Board({roomID, gameData, colour, opponentLeft, gameStartConfirmation, boardHistory, viewMoveHistory}) {
    const boardRef = useRef(document.createElement('div'));
    const username = localStorage.getItem("username");



    var blackPawns, blackPieces, whitePawns, whitePieces 
    // gameData[gameData.player1].colour == 'black' ? (blackPawns = 1, whitePawns = 6) : (blackPawns = 6, whitePawns = 1);
    // gameData[gameData.player1].colour == 'black' ? (blackPieces = 0, whitePieces = 7) : (blackPieces = 7, whitePieces = 0);
    // if (gameData[gameData.player1].username == username) {
    //     blackPawns = 1
    //     whitePawns = 6
    //     blackPieces = 0
    //     whitePieces = 7
    // }
    // else {
    //     blackPawns = 6
    //     whitePawns = 1
    //     blackPieces = 7
    //     whitePieces = 0
    // }

    

    const [pieces, setPieces] = useState(startBoardState)
    const [currentPiece, setCurrentPiece] = useState(document.createElement('div'));
    const [boardX, setBoardX] = useState(0);
    const [boardY, setBoardY] = useState(0);
    const [validMoves, setValidMoves] = useState(false);
    const socket = useContext(SocketContext)
    let navigate = useNavigate()
    const [history, setHistory] = useState([])
    const [didBoardChange, setDidBoardChange] = useState()
    const [canIMove, setCanIMove ] = useState(true)
    const [moves, setMoves] = useState([])
    const [isGameFinished, setIsGameFinished] = useState(false)
    const [blackTakenPieces, setBlackTakenPieces] = useState([])
    const [whiteTakenPieces, setWhiteTakenPieces] = useState([])
    const [statusMessage, setStatusMessage] = useState("")
    const [isDrawOffered, setIsDrawOffered] = useState(false)
    const [whiteScore, setWhiteScore] = useState(0)
    const [blackScore, setBlackScore] = useState(0)
    const [score, setScore] = useState(0)
    // const [whiteScore, setWhiteScore] = useState(0)
    // const [blackScore, setBlackScore] = useState(0)
    // const [whoseTurn, setWhoseTurn] = useState('white')
    let currentState
    let myArray = []

    let colourSwitch = new Map([['white', 'black'], ['black', 'white']])

    

    

    // useEffect(() => {
    //     socket.on('addToHistory', ({boardState}) => {
    //         
    //         
    //         setHistory(history => [...history, boardState ] )
    //         // handleHistory(boardState)
    //     })
    //   }, [didBoardChange]);

      useEffect(() => {
        

        const eventListener = ({boardState, move, takenPiece}) => {
            setHistory((history) => [...history, boardState ] )
            
            setMoves((moves) => [...moves, move ] )

            if (takenPiece.length !== 0 && takenPiece.teamType === "white") {
                setWhiteTakenPieces((whiteTakenPieces) => [...whiteTakenPieces, takenPiece])
                if (takenPiece.pieceType === 1) setBlackScore((blackScore) => blackScore + 1)
                if (takenPiece.pieceType === 2) setBlackScore((blackScore) => blackScore + 3)
                if (takenPiece.pieceType === 3) setBlackScore((blackScore) => blackScore + 3)
                if (takenPiece.pieceType === 5) setBlackScore((blackScore) => blackScore + 5)
                if (takenPiece.pieceType === 9) setBlackScore((blackScore) => blackScore + 9)
                
                

            }
            if (takenPiece.length !== 0 && takenPiece.teamType === "black") {
                setBlackTakenPieces((blackTakenPieces) => [...blackTakenPieces, takenPiece])
                if (takenPiece.pieceType === 1) setWhiteScore((whiteScore) => whiteScore + 1)
                if (takenPiece.pieceType === 2) setWhiteScore((whiteScore) => whiteScore + 3)
                if (takenPiece.pieceType === 3) setWhiteScore((whiteScore) => whiteScore + 3)
                if (takenPiece.pieceType === 5) setWhiteScore((whiteScore) => whiteScore + 5)
                if (takenPiece.pieceType === 9) setWhiteScore((whiteScore) => whiteScore + 9)
                
            }
            // if (whiteScore > blackScore) {
            //     setScore(whiteScore - blackScore)
            // } else {
            //     setScore(blackScore - whiteScore)
            // }
            
            
        };

        socket.on("addToHistory", eventListener )

        
        
    
        return () => socket.off("addToHistory", eventListener);
      }, [didBoardChange]);

    //   useEffect(() => {
        
    //     const test = ({roomID, history}) => {

    //     }

    //     // socket.on("addToHistory", eventListener )
    //     socket.emit('submitHistory', test)
        
    
    //     // return () => socket.off("addToHistory", eventListener);
    //   }, [isGameFinished]);

    
    
    useEffect(() => {

        

        socket.on('update', ({boardState, whoseTurn}) => {
            // setWhoseTurn(colourSwitch.get(colour)
            setCanIMove(true)
            handleUpdate(boardState)
            setDidBoardChange(true)
            setIsDrawOffered(false)
            // calcScores()
            
            
        })

        socket.on('winner', ({boardState, teamColour, method}) => {
            handleUpdate(boardState)
            setIsGameFinished(true)
            // window.alert('mate:' + teamColour)
            setStatusMessage(teamColour + ' wins by ' + method)
            setValidMoves({})
        
        })

        socket.on('restart', (data) => {

            handleUpdate(startBoardState)
            setIsGameFinished(false)
            setHistory([])
            gameStartConfirmation(data);
        });

        

        socket.on('draw', ({boardState}) => {
            handleUpdate(boardState)
            setIsGameFinished(true)
            setIsDrawOffered(false)
            setStatusMessage('Game ends in stalemate')
            setValidMoves({})
        })

        // socket.on('opponentLeft', () => {
        //     //PUT THIS BACK IN
        //     // window.alert('opponent left')
        //     // navigate('/home')
        //     opponentLeft()
        // })

        socket.on('returnValidMoves', (data) => {
            setValidMoves(data)
        })

        socket.on('drawOffered', (data) => {
            setIsDrawOffered(true)
        })

        if (boardHistory) {
            setHistory(boardHistory.map( item => {return JSON.parse(item.boardState)}))
            setMoves(boardHistory.map( item => {return JSON.parse(item.moves)}))            
            handleUpdate(JSON.parse(boardHistory[boardHistory.length - 1].boardState))
            
        } else {
            window.onpopstate = () => {
                resign()
                goBack()
            }
        }

        socket.on('opponentLeft', () => {
            
            window.alert('Opponent left - redirecting to home screen')
            navigate('/home')
        })

        // if (takenPieces) {
        //     // for (var p in takenPieces) {
        //     //     if (p.teamType === "black") {
        //     //         setTakenBlackPieces()
        //     //     }
        //     // }
        //     var blackTakenPieces = this.takenPieces.filter((piece) => piece.teamType === "black")
        //     
        // }

        return () => {
            socket.off('opponentLeft')
        }

    }, [])

    

    useEffect(() => {
        if (!roomID) return;
        
        socket.emit('startBoardState', ({startBoardState, roomID}))
        setStatusMessage('Game currently ongoing')

        // setHistory(startBoardState)
    }, [roomID])

    // useEffect(() => {
    //     window.addEventListener('keydown', e => {
    //         if(e.key === 'ArrowLeft'){
    //             setCurrentMove(moves.length)
    //             
    //             
    //             jumpTo(4)
    //         }
    //     })
    // })


    function submitHistory() {
        
    }

    

    if (isGameFinished) {
        
        if (colour === "black") {
            socket.emit('submitHistory', ({roomID, history, moves}))
        }
    }
    // } else {
    //     setStatusMessage('Game currently ongoing')
    // }
        

    
    // 

    // if (roomID) {
    //     socket.emit('startBoardState', ({startBoardState, roomID}))
    // }

    function handleUpdate(boardState){
        // setHistory(history => ({
        //     myArray: [...history, boardState]
        // }))
        // 
        // 
        // setValidMove(validMove)
        // setCurrentMove(move)
        
        setPieces(boardState)
    }

    

    function grabPiece(e) {
        const element = e.target;
        const board = boardRef.current;
        if (canIMove && !isGameFinished) {
            if (element.classList.contains("chess-piece-" + colour) && board) {
                setBoardX(Math.floor((e.clientX - board.offsetLeft) / 75));
                setBoardY(Math.abs(Math.ceil((e.clientY - board.offsetTop - 600) / 75)));
                
    
                const x = e.clientX - 37.5;
                const y = e.clientY - 37.5;
                element.style.position = "absolute";
                element.style.left = `${x}px`;
                element.style.top =`${y}px`;
                let currentX = Math.floor((e.clientX - board.offsetLeft) / 75)
                let currentY = Math.abs(Math.ceil((e.clientY - board.offsetTop - 600) / 75))
                socket.emit('getValidMoves', ({x: currentX, y: currentY, roomID: roomID, colour: colour}))
    
                setCurrentPiece(element);
                }
        }
        // if (element.classList.contains("chess-piece-" + colour) && board) {
        //     setBoardX(Math.floor((e.clientX - board.offsetLeft) / 75));
        //     setBoardY(Math.abs(Math.ceil((e.clientY - board.offsetTop - 600) / 75)));
            

        //     const x = e.clientX - 37.5;
        //     const y = e.clientY - 37.5;
        //     element.style.position = "absolute";
        //     element.style.left = `${x}px`;
        //     element.style.top =`${y}px`;
        //     let currentX = Math.floor((e.clientX - board.offsetLeft) / 75)
        //     let currentY = Math.abs(Math.ceil((e.clientY - board.offsetTop - 600) / 75))
        //     
        //     socket.emit('getValidMoves', ({x: currentX, y: currentY, roomID: roomID, colour: colour}))

        //     setCurrentPiece(element);
        //     }
    }

    function movePiece(e) {
        //const element = e.target as HTMLElement;
        const board = boardRef.current;
        if (currentPiece && board) {
            const minX = board.offsetLeft-10;
            const minY = board.offsetTop-10;
            const maxX = board.offsetLeft + board.clientWidth - 65;
            const maxY = board.offsetTop + board.clientHeight - 75;
            const x = e.clientX - 37.5;
            const y = e.clientY - 37.5;
            currentPiece.style.position = "absolute";
            currentPiece.style.left = `${x}px`;
            currentPiece.style.top =`${y}px`;
        
            

            if (x < minX) {
                currentPiece.style.left = `${minX}px`;
            }
            else if (x > maxX) {
                currentPiece.style.left = `${maxX}px`;
            }
            else {
                currentPiece.style.left = `${x}px`;
            }
            if (y < minY) {
                currentPiece.style.top = `${minY}px`;
            }
            else if (y > maxY) {
                currentPiece.style.top = `${maxY}px`;
            }
            else {
                currentPiece.style.top = `${y}px`;

            }   
        }
    }

    function calculateAbs(value) {
        return Math.abs(value - 7)
    }

    function dropPiece(e) {
        const board = boardRef.current;
        if (currentPiece && board) {
            
            //if colour is black, change these to absolute - 7? to make neater
            const x = Math.floor((e.clientX - board.offsetLeft) / 75);
            const y = Math.abs(Math.ceil((e.clientY - board.offsetTop - 600) / 75));
            

            setPieces((value) => {
                const pieces = value.map((p) => {
                    if (colour === "white") {
                        if (p.x === boardX && p.y === boardY) {
                                // let move = {currentX: boardX, currentY: boardY, targetX: x, targetY: y, piece: p.pieceType, team: p.teamType, roomID: roomID, boardState: value} 
                                let move = {currentX: boardX, currentY: boardY, targetX: x, targetY: y, piece: p.pieceType, team: p.teamType, roomID: roomID}  
                                
                                socket.emit('move', ({move, username}))
                                // setCurrentMove(move)
                                // 
                                // if (validMove) {
                                    
                                //     
                                //     // p.x=x;
                                //     // p.y=y;
                                    
                                // }
                                // else {
                                        currentPiece.style.position = 'relative';
                                        currentPiece.style.removeProperty('top');
                                        currentPiece.style.removeProperty('left');
                                        
                                    // }
                        }
                    }
                    else {
                        if (p.x === calculateAbs(boardX) && p.y === calculateAbs(boardY)) {
                            // let move = {currentX: calculateAbs(boardX), currentY: calculateAbs(boardY), targetX: calculateAbs(x), targetY: calculateAbs(y), piece: p.pieceType, team: p.teamType, roomID: roomID, boardState: value}
                            let move = {currentX: calculateAbs(boardX), currentY: calculateAbs(boardY), targetX: calculateAbs(x), targetY: calculateAbs(y), piece: p.pieceType, team: p.teamType, roomID: roomID}
                            socket.emit('move', ({move, username}))
                            
                            // if (validMove) {
                                // 
                                // setCurrentMove(move)
                                // 
                                // p.x=calculateAbs(x);
                                // p.y=calculateAbs(y);
                            // } else {
                                currentPiece.style.position = 'relative';
                                currentPiece.style.removeProperty('top');
                                currentPiece.style.removeProperty('left');
                                
            
                            // }
                    }   
                }
                
                return p;
                });
            
            return pieces;
            });
            setCurrentPiece(null);
        }
    }

    let board = [];
        for (let j = verticalAxis.length - 1; j >= 0; j--) {
            for (let i = 0; i < horizontalAxis.length; i++) {

                const whiteBlack = i + j;
                let image = undefined;
                let position = horizontalAxis[i] + verticalAxis[j];

                let absi = Math.abs(i - 7)
                let absj = Math.abs(j - 7)
                if (colour == "black") {
                    pieces.forEach((p) => {
                        if (p.x === absi && p.y === absj) {
                            image = p.image;
                        }
                    }); 
                } else if (colour == "white" ) {
                    pieces.forEach((p) => {
                        if (p.x === i && p.y === j) {
                            image = p.image;
                         }

                    });
                }
                let potentialMove = false
                for (var m in validMoves) {
                    if (i === validMoves[m].x && j === validMoves[m].y) {
                        potentialMove = true
                    } 
                } 
                
                board.push(<Square key={`${i},${j}`} whiteBlack={whiteBlack} position={position} image={image} colour={colour} potentialMove={potentialMove}/>)
                potentialMove = false
            }
        }



// const horizontalAxis = ["A", "B", "C", "D", "E", "F", "G", "H"]
// const verticalAxis = ["1", "2", "3", "4", "5", "6", "7", "8"];
// if (!boardHistory) {
    
        const moveHistory = history.map((step, move) => {
            

           
            const currentSquare = horizontalAxis[moves[move].currentX] + verticalAxis[moves[move].currentY]
            const targetSquare = horizontalAxis[moves[move].targetX] + verticalAxis[moves[move].targetY]
            let movedPiece
            let movedColour = moves[move].team
            

            if (moves[move].piece === 1) movedPiece = 'pawn'
            if (moves[move].piece === 2) movedPiece = 'knight'
            if (moves[move].piece === 3) movedPiece = 'bishop'
            if (moves[move].piece === 5) movedPiece = 'rook'
            if (moves[move].piece === 9) movedPiece = 'queen'
            if (moves[move].piece === 10) movedPiece = 'king'
            const desc = 'Go to move ' + (move + 1) + '(' + movedColour + ' ' + movedPiece + ': ' + currentSquare + '->' + targetSquare +  ')'
            // const desc = 'Go to move ' + (move + 1) 
            return (
              <li>
                <button onClick={() => jumpTo(move + 1)}>{desc}</button>
              </li>
            );
          });
        // }

          
    
    function jumpTo(move) {
        
        // 
        setPieces(history[move - 1])
        if (move === history.length) {
            setCanIMove(true)}
        else {
            setCanIMove(false)
        }
        
    }

    function rematchRequest() {
        setBlackTakenPieces([])
        setWhiteTakenPieces([])
        setWhiteScore(0)
        setBlackScore(0)
        socket.emit('rematchRequest', ({startBoardState, roomID}))
    }

    function resign() {
        setIsGameFinished(true)
        socket.emit('resign', ({roomID, colour}))
    }

    function offerDraw() {
        socket.emit('offerDraw', ({roomID, colour}))
    }

    function acceptDraw() {
        socket.emit('acceptDraw', ({roomID}))
    }

    function goBack() {
        socket.emit('leaveGame', ({roomID}))
        // socket.disconnect()
        navigate('/home')
        //do opponentleft()
    }

    
    

    function calcScores() {
        // let blackScore = 0, whiteScore = 0
        
        
        // setBlackScore(blackScore - whiteScore)
        // setWhiteScore(whiteScore - blackScore)
    }
    
    console.log(whiteTakenPieces)
    console.log(blackTakenPieces)
    
    console.log(whiteScore)
    console.log(blackScore)

    // setStatusMessage('Game currently ongoing')
    
    return(
    <Row>

        <div className='status'>
            <h2 className="status-message" style={{textAlign:"center"}}>{statusMessage}</h2>
        </div>
        <div>
        {colour === "white" ?
        <TakenPieces takenPieces={whiteTakenPieces} score={blackScore - whiteScore}></TakenPieces> :
        <TakenPieces takenPieces={blackTakenPieces} score={whiteScore - blackScore}></TakenPieces>
        }
        </div>
        {/* <TakenPieces takenPieces={whiteTakenPieces}></TakenPieces> */}
        <Col>
       
        {boardHistory ?
        <div ref={boardRef} id="board">
            {board}
        </div>
        :
        <div 
            onMouseUp = {e => dropPiece(e)} 
            onMouseMove = {e => movePiece(e)} 
            onMouseDown ={e => grabPiece(e) } 
            id="board"
            ref={boardRef}  
            >  
            {board}
        </div>
        }
        <div>
        {colour === "white" ?
            <TakenPieces takenPieces={blackTakenPieces} score={whiteScore - blackScore}></TakenPieces> :
            <TakenPieces takenPieces={whiteTakenPieces} score={blackScore - whiteScore}></TakenPieces>
        }  
        </div>
        </Col>
        <Col>
        {boardHistory ?
        <p></p> :
        <div>
        <button className = 'resign-button' style={{visibility: !isGameFinished?'visible':'hidden', opacity: !isGameFinished?'1':'0'}} onClick={resign}>Resign</button>
            <br></br>
            <button className = 'offerDraw-button' style={{visibility: !isGameFinished?'visible':'hidden', opacity: !isGameFinished?'1':'0'}} onClick={offerDraw}>Offer Draw</button>
            <br></br>
            <button className = 'acceptDraw-button' style={{visibility: isDrawOffered?'visible':'hidden', opacity: isDrawOffered?'1':'0'}} onClick={acceptDraw}>Accept Draw</button>
        </div>
        }
            
        </Col>
        <Col>
        {/* {score} */}
        {/* <TestTimer />
        <TestTimer /> */}
        {/* {colour === "white" ? } */}
            {/* <Timer canIMove={canIMove}/> */}
            {/* <br></br>
            <Timer canIMove={!canIMove}/> */}
        </Col>
         <Col>
            <ol>{moveHistory}</ol>
        </Col>
        {boardHistory ?
        <p></p> :
        <div className='rematch-container'>
            <button className='rematch-button' onClick={rematchRequest} style={{visibility: isGameFinished?'visible':'hidden', opacity: isGameFinished?'1':'0'}}>Play Again</button>
            <button className='goBack-button' onClick={goBack} style={{visibility: isGameFinished?'visible':'hidden', opacity: isGameFinished?'1':'0'}}>Go back to home page</button>
        </div>
        }
        
    </Row> 
    )
}