import React from "react";
import io from "socket.io-client";
import socketIOClient, { Socket } from 'socket.io-client';


const ENDPOINT = "http://127.0.0.1:6001";

// const ENDPOINT = "https://chess-server-final.herokuapp.com";


const accessToken = localStorage.getItem('accessToken')?.slice(1, -1)


export const socket = io.connect(ENDPOINT, {
    query: 'token=' + accessToken
});


export const SocketContext = React.createContext(socket);

