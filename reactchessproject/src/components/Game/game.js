import React, {useState,setState, useEffect} from 'react';
import Board from '../../components/Board/Board';

import axios from 'axios';
import { Row, Col, Container, Card, Button } from 'react-bootstrap'
import socketIOClient from 'socket.io-client';
const ENDPOINT = "http://127.0.0.1:6001";


function Game() {
    
    const [test, setTest] = useState("")


    useEffect(() => {
      const socket = socketIOClient("http://127.0.0.1:6001/createGame");
      socket.on('connectToRoom', (data) => {
        setTest(data)
        console.log(data)
      })
    },[])

    // console.log(onlineFriends.indexOf('finnerstighe'))

    return(

        <Board />
       
    )       
}

export default Game

