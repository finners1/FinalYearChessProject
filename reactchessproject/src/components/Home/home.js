import React, {useState,setState, useEffect} from 'react';
import Board from '../../components/Board/Board';
import  './home.css'
import axios from 'axios';
// import Col from 'react-bootstrap/Col';
// import Container from 'react-bootstrap/Container';
// import Row from 'react-bootstrap/Row';
import { Row, Col, Container, Card, Button } from 'react-bootstrap'
import socketIOClient, { Socket } from 'socket.io-client';
import { useNavigate } from "react-router-dom";

import { SocketContext } from "../Context/socket";
import OpponentList from '../Opponents/opponentList';

// const ENDPOINT = "http://127.0.0.1:6001";
// const socket = socketIOClient(ENDPOINT);

function Home({gameStartConfirmation, colourAssignment, isGameStarted}) {

    const [friends, setFriends] = useState([])
    const [currentUser, setCurrentUser] = useState(null)
    const [onlineFriends, setOnlineFriends] = useState([])
    const [online, setOnline] = useState("")
    const [opponents, setOpponents] = useState([])
    const socket = React.useContext(SocketContext)
    let navigate = useNavigate();

    const username = localStorage.getItem("username");
    const accessToken = localStorage.getItem("accessToken");

    // console.log(username)
    

    const fetchData = () => {
      // axios.get('https://db-server-final.herokuapp.com/getFriends', {
      axios.get('http://localhost:5001/getFriends', {        
          params: {
            username: username,
            token : accessToken
          }
          })
            .then((res) => {
                setFriends(res.data)
                // console.log("aaaa")
            }).catch((error) => {
                console.log(error)
            });

      // axios.get('https://db-server-final.herokuapp.com/getCurrentUser', {
      axios.get('http://localhost:5001/getCurrentUser', {
        params: {
          username: username,
          token: accessToken
        } 
        })
          .then((res) => {
              setCurrentUser(res.data)
              // console.log(res.data)
          }).catch((error) => {
              console.log(error)
          });
      
    }


    // useEffect(() => {
    //   socket.on('gameRequest', (data) => {
    //     if (window.confirm('Player is challenging you')) {
    //         socket.emit('gameRequestAccepted', {"id": data.id})
    //     }
    // })

    // return () => socket.off('gameRequest')
    // }, [])

    // useEffect(() => {
    //   fetchData()
    // }, [])

    useEffect(() => {
      fetchData()
      

      socket.emit('sendUsername', {username: username})
      socket.emit('getOpponents', {});

      socket.on('gameRequest', (data) => {
        if (window.confirm('Player is challenging you')) {
            socket.emit('gameRequestAccepted', {"id": data.id})
        }
    })

      // socket.once('connectedUsers', (data) => {
      //   setOnlineFriends(data) 
      //   // console.log(data)
      // })

      const eventListener = (data) => {
            
        setOpponents((opponents) => [...opponents, data])
    };

      socket.on("newOpponentAdded", eventListener )      

      return () => {
        socket.off('gameRequest')
        socket.off("newOpponentAdded", eventListener);
      } 
    },[])

    socket.on('getOpponentsResponse', data => {
      setOpponents(data)
    }); 

    socket.on('opponentDisconnected', data => {
      var flag = false;
      var i = 0;
      for (i = 0; i < opponents.length; i++) {
          if (opponents[i].id === data.id) {
              flag = true;
              break;
          }
      }
      if (flag) {
          var array = [...opponents];
          array.splice(i, 1);
          setOpponents(array);
      }
  });

  socket.on('excludePlayers', data => {
      for (var j = 0; j < data.length; j++) {
          var flag = false;
          var i = 0;
          for (i = 0; i < opponents.length; i++) {
              if (opponents[i].id === data[j]) {
                  console.log(opponents[i].id, data[j])
                  flag = true;
                  break;
              }
          }
          if (flag) {
              var array = [...opponents];
              array.splice(i, 1);
              setOpponents(array);
          }
      }
  });

  socket.on('gameStarted', data => {
    navigate('/game')
    gameStartConfirmation(data);
  });

  socket.on('colourAssignment', data => {
      colourAssignment(data)
  })

  function selectOpponent(opponentUsername) {
    console.log(opponentUsername)
      // console.log(opponents[index])
      var selectIndex
      for (var i in opponents) {
        if (opponents[i].username === opponentUsername) {
          selectIndex = i
        }
      }
      console.log(selectIndex)
      socket.emit('selectOpponent', { "id": opponents[selectIndex].id });
      
  };

    for (var i = 0; i < opponents.length; i++) {
      for (var j = 0; j < friends.length; j++) {
        if (opponents[i].username == friends[j].username) {
          friends[j].online = true;
        }
        else {
          friends[j].online = false;
        }
      }
    }

    const goToProfile  = () => {
      socket.emit('viewProfile')
      navigate('/profile')
    }


    const addFriends = () => {
      navigate('/addFriends')
    }
    return(
  <Container>
    <Row>
    <Col sm={3}>
      
    <h1>Friends</h1> 
      {friends ? friends.map((friend, index) => (
      /* {Object.values(friends).map(friend => ( */
        <Container>
        <Card key={friend.username} bg = 'info' style={{ width: '12rem' }}>
          <Card.Body>
            <Card.Title>{friend.username}</Card.Title>
            <Card.Subtitle className="mb-2 text-muted">{friend.firstName} {friend.lastName}</Card.Subtitle>
            <Card.Text> {friend.online ? 'online' : 'offline'} </Card.Text>
            {friend.online ? 
              <Button variant = "success" onClick = {()=>selectOpponent(friend.username)} key={index}>Send Challenge</Button>
            :
              <p></p>} 
            
          </Card.Body>
        </Card>
        <p></p><p></p>
      </Container>
      )) :
      <p></p>}
      <Button variant = "warning" onClick={addFriends}>Add Friend</Button>
          
    </Col>
    <Col sm={6}>
        <h2>ONLINE USERS</h2>
        <OpponentList gameStartConfirmation={gameStartConfirmation} colourAssignment={colourAssignment} opponents={opponents}/>
        
        
        
    </Col>
    <Col sm={3}>
        <Card>
          <Card.Body>
            <Card.Title>
              {username}
            </Card.Title>
            <p></p>
            {currentUser ? (
              <Card.Subtitle>
                {currentUser[0].firstName} {currentUser[0].lastName}
              </Card.Subtitle>
            ) : (
              <Card.Subtitle>
                Loading...
              </Card.Subtitle>
            )}
            <p></p>
              <Button variant="info" onClick={goToProfile}>
                View Profile
              </Button>
          </Card.Body>
        </Card>
    </Col>
    </Row>
  </Container>
       
    )       
}

export default Home

