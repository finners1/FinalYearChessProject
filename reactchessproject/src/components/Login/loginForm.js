import React, {useState,setState} from 'react';
import '../Registration/register.css'
import axios from 'axios';
import { Link, Route } from "react-router-dom";
import { useNavigate } from "react-router-dom";
import Board from '../Board/Board';

function LoginForm() {

    const [username, setUsername] = useState("");
    const [password,setPassword] = useState("");
    let navigate = useNavigate(); 
    const accessToken = localStorage.getItem("accessToken")

    const handleInputChange = (e) => {
        const {id , value} = e.target;
        if(id === "username"){
            setUsername(value);
        }
        if(id === "password"){
            setPassword(value);
        }
    }

    const handleSubmit  = () => {
        const userObject = {
            "username": username,
            "password": password,
            "token" : accessToken
        };

        axios.post('http://localhost:5001/login', userObject)
            .then((res) => {
                localStorage.setItem('accessToken', JSON.stringify(res.data['accessToken']))
                localStorage.setItem('refreshToken', JSON.stringify(res.data['refreshToken']))
                localStorage.setItem('username', username)
                navigate("/home");
  
            }).catch((error) => {
                console.log(error)
            });

    }

    return(
        <div className="form">
            <div className="form-body">
                <div className="username">
                    <label className="form__label" htmlFor="username">Username </label>
                    <input  type="text" name="" id="username" value={username} onChange = {(e) => handleInputChange(e)} placeholder="Username"/>
                </div>
                <div className="password">
                    <label className="form__label" htmlFor="password">Password </label>
                    <input className="form__input" type="password"  id="password" value={password} onChange = {(e) => handleInputChange(e)} placeholder="Password"/>
                </div>
            </div>
            <div className="footer">
                <button onClick={()=>handleSubmit()} type="submit" className="btn">Login</button>
            </div>
            {/* <Board /> */}
        </div>
       
    )       
}

export default LoginForm