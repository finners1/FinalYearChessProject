import React, {useState,setState, useEffect, useContext} from 'react';
import Board from '../../components/Board/Board';
import axios from 'axios';
import { ListGroup } from 'react-bootstrap'
import socketIOClient from 'socket.io-client';
import { useNavigate } from "react-router-dom";
import { SocketContext } from "../Context/socket";
import history from '../../history';

function OpponentList( {gameStartConfirmation, colourAssignment, isGameStarted}) {

    const [opponents, setOpponents] = useState([])
    let navigate = useNavigate();
    const socket = React.useContext(SocketContext);
    const [selectedOpponent, setSelectedOpponent] = useState()

    const username = localStorage.getItem("username");



    useEffect(() => {
        
        const eventListener = (data) => {
            
            setOpponents((opponents) => [...opponents, data])
        };

        socket.on("newOpponentAdded", eventListener )

        
        
    
        return () => socket.off("newOpponentAdded", eventListener);
      }, []);



    useEffect(() => {





        socket.emit('sendUsername', {username: username})
        socket.emit('getOpponents', {});
        socket.on('gameRequest', (data) => {
            if (window.confirm('Player is challenging you')) {
                socket.emit('gameRequestAccepted', {"id": data.id})
            }
            
        })

        return () => {
            socket.off('gameRequest')
        }
    
    } , [])

    socket.on('getOpponentsResponse', data => {
        setOpponents(data)
    });  

    socket.on('gameStarted', data => {
        navigate('/game')
        gameStartConfirmation(data);
    });

    socket.on('opponentDisconnected', data => {
        var flag = false;
        var i = 0;
        for (i = 0; i < opponents.length; i++) {
            if (opponents[i].id === data.id) {
                flag = true;
                break;
            }
        }
        if (flag) {
            var array = [...opponents];
            array.splice(i, 1);
            setOpponents(array);
        }
    });

    socket.on('excludePlayers', data => {
        console.log(data);
        for (var j = 0; j < data.length; j++) {
            var flag = false;
            var i = 0;
            for (i = 0; i < opponents.length; i++) {
                if (opponents[i].id === data[j]) {
                    console.log(opponents[i].id, data[j])
                    flag = true;
                    break;
                }
            }
            if (flag) {
                console.log('if flag')
                var array = [...opponents];
                array.splice(i, 1);
                setOpponents(array);
                console.log(array)
            }
        }
    });

    socket.on('colourAssignment', data => {
        colourAssignment(data)
    })

    function selectOpponent(index) {
        socket.emit('selectOpponent', { "id": opponents[index].id });
        
    };

    return( 
        
        <ListGroup onSelect = {selectOpponent}>
        {Object.values(opponents).map( (opponent, index) => (
            <ListGroup.Item action={true} className="opponent-item" key={index} eventKey={index}> {opponent.username} {index}</ListGroup.Item>
        ))}
        </ListGroup>
    )       
}

export default OpponentList
