import React, {useState,setState, useEffect, useContext} from 'react';
import Board from '../../components/Board/Board';
import axios from 'axios';
// import Col from 'react-bootstrap/Col';
// import Container from 'react-bootstrap/Container';
// import Row from 'react-bootstrap/Row';
import { ListGroup } from 'react-bootstrap'
import socketIOClient from 'socket.io-client';
import { useNavigate } from "react-router-dom";
import { SocketContext } from "../Context/socket";
import history from '../../history';

// const ENDPOINT = "http://127.0.0.1:6001";
// const socket = socketIOClient(ENDPOINT);

function OpponentList( {gameStartConfirmation, colourAssignment, opponents}) {

    // const [opponents, setOpponents] = useState([])
    let navigate = useNavigate();
    const socket = React.useContext(SocketContext);

    useEffect(() => {
        // socket.on('gameRequest', (data) => {
        //     if (window.confirm('Player is challenging you')) {
        //         socket.emit('gameRequestAccepted', {"id": data.id})
        //     }
        // })

        // return () => {
        //     socket.off('gameRequest')
        // }
    } , [])

    socket.on('gameStarted', data => {
        navigate('/game')
        gameStartConfirmation(data);
    });

    socket.on('colourAssignment', data => {
        colourAssignment(data)
    })

    function selectOpponent(index) {
        socket.emit('selectOpponent', { "id": opponents[index].id });
        
    };

    return( 
        <ListGroup onSelect = {selectOpponent}>
        {Object.values(opponents).map( (opponent, index) => (
            <ListGroup.Item action={true} className="opponent-item" key={index} eventKey={index}> {opponent.username} </ListGroup.Item>
        ))}
        </ListGroup>
    )       
}

export default OpponentList
