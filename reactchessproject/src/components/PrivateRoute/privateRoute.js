import { Navigate } from "react-router-dom";

const PrivateRoute = ({ children }) => {
    const token = localStorage.getItem("accessToken");

    
    if (token === "undefined") {
      return <Navigate to="/login" replace />;
    }
  
    return children;
  };

export default PrivateRoute