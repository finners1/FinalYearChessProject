import React, {useState,setState, useEffect} from 'react';

import axios from 'axios';
import { useNavigate } from "react-router-dom";
import { Row, Col, Container, Card, Button, ListGroup } from 'react-bootstrap'
import Board from '../../components/Board/Board';

function Profile({viewGameConfirmation, leaveViewGame}) {

    const [previousGames, setPreviousGames] = useState("");
    const [previousStates, setPreviousStates] = useState("");
    const [wins, setWins] = useState("");
    const [losses, setLosses] = useState("");
    const [draws, setDraws] = useState("");
    const accessToken = localStorage.getItem("accessToken")

    let navigate = useNavigate(); 

    const username = localStorage.getItem("username");

    useEffect(() => {
        // axios.get('https://db-server-final.herokuapp.com/getPreviousGames', {
        axios.get('http://localhost:5001/getPreviousGames', {    
        params: {
            username: username,
            token : accessToken
          } 
        }).then((res) => {
            console.log(res)
            setPreviousGames(res.data)
        }).catch((error) => {
            console.log(error)
        })

        // axios.get('https://db-server-final.herokuapp.com/getWLD', {
        axios.get('http://localhost:5001/getWLD', {
        params: {
            username: username,
            token : accessToken
          } 
        }).then((res) => {
            // console.log(res.data['wins'][0]['COUNT(winner)'])
            // console.log(res.data['losses'][0]['COUNT(winner)'])
            // console.log(res.data['draws'][0]['COUNT(winner)'])
            setWins(res.data['wins'][0]['COUNT(winner)'])
            setLosses(res.data['losses'][0]['COUNT(winner)'])
            setDraws(res.data['draws'][0]['COUNT(winner)'])
        }).catch((error) => {
            console.log(error)
        })

        window.onpopstate = () => {
            leaveViewGame()
        }

    }, [])

    console.log('winner: '+ previousGames.winner)

    function selectGame(index) {
        //TODO: query prev states database wtih gameID
        //? redirect to new pge showing final state?
        //or just render a board component? with isgamefinsihed set to true
        let selectedGame = previousGames[index]
        let myColour
        // axios.get('https://db-server-final.herokuapp.com/getGameStates', {
        axios.get('http://localhost:5001/getGameStates', {
        params: {
            gameID: previousGames[index].gameID,
            token : accessToken
          } 
        }).then((res) => {
            console.log(res.data)
            setPreviousStates(res.data)
            if (selectedGame.winner === username) {
                myColour = selectedGame.winnerColour
            }
            if (selectedGame.loser === username) {
                myColour = selectedGame.loserColour
            }
            viewGameConfirmation(res.data, myColour);
        }).catch((error) => {
            console.log(error)
        })
        console.log(previousStates)
        
    };


    
    if (previousGames) {
        return(
            <div>
            <h1>Previous Games</h1>
            <h2 style={{textAlign:'center'}}>Wins: {wins}&nbsp;&nbsp;&nbsp; Losses: {losses} &nbsp;&nbsp;&nbsp;Draws: {draws}</h2>
            <ListGroup onSelect = {selectGame}>
            {Object.values(previousGames).map( (game, index) => (
                (game.draw === 1) ? <ListGroup.Item variant = "warning"action={true} className="opponent-item" key={index} eventKey={index}> Player 1: {game.winner} Player 2: {game.loser} - ended in draw</ListGroup.Item> :
                (game.winner === username) ?
                <ListGroup.Item variant="success" action={true} className="opponent-item" key={index} eventKey={index}> Winner: {game.winner} Loser: {game.loser}</ListGroup.Item> :
                <ListGroup.Item variant="danger" action={true} className="opponent-item" key={index} eventKey={index}> Winner: {game.winner} Loser: {game.loser}</ListGroup.Item>
            ))}
            </ListGroup>
            </div>
           
        )  
    }   
   

}

export default Profile