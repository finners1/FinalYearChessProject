import React, {useState,setState} from 'react';
import './register.css'
import axios from 'axios';
import { useNavigate } from "react-router-dom";
import DatePicker from "react-date-picker";

function RegistrationForm() {
    
    const [firstName, setFirstName] = useState("");
    const [lastName, setLastName] = useState("");
    const [username, setUsername] = useState("");
    const [email, setEmail] = useState("");
    const [password,setPassword] = useState("");
    const [confirmPassword,setConfirmPassword] = useState("");
    const [birthDate, setBirthDate] = useState(new Date());
    var dateMinus18 = new Date();
    dateMinus18.setFullYear(dateMinus18.getFullYear() - 18);

    let navigate = useNavigate();

    const handleInputChange = (e) => {
        const {id , value} = e.target;
        if(id === "firstName"){
            setFirstName(value);
        }
        if(id === "lastName"){
            setLastName(value);
        }
        if(id === "username"){
            setUsername(value);
        }
        if(id === "email"){
            setEmail(value);
        }
        if(id === "password"){
            setPassword(value);
        }
        if(id === "confirmPassword"){
            setConfirmPassword(value);
        }
    }

    const onChange = (date) => {
        setBirthDate(date);
      }

    const handleSubmit  = () => {
        if (password !== confirmPassword) {
            alert("Passwords do not match");
            return;
        }
        if (!firstName || !lastName || !username || !email || !password || !confirmPassword) {
            alert("Please fill in all the boxes");
            return;
        }
        const userObject = {
            "firstName": firstName,
            "lastName": lastName,
            "username": username,
            "email": email,
            "birthDate": birthDate,
            "password": password
        };
        // axios.post('https://db-server-final.herokuapp.com/createUser', userObject)
        axios.post('http://localhost:5001/createUser', userObject)
            .then((res) => {
                console.log(res.data)
                navigate("/login");
            }).catch((error) => {
                console.log(error)
            });
    }

    return(
        <div className="form">
            <div className="form-body">
                <div className="firstname">
                    <label className="form__label" htmlFor="firstName">First Name </label>
                    <input className="form__input" type="text" value={firstName} onChange = {(e) => handleInputChange(e)} id="firstName" placeholder="First Name"/>
                </div>
                <div className="lastname">
                    <label className="form__label" htmlFor="lastName">Last Name </label>
                    <input  type="text" name="" id="lastName" value={lastName}  className="form__input" onChange = {(e) => handleInputChange(e)} placeholder="Last Name"/>
                </div>
                <div className="username">
                    <label className="form__label" htmlFor="username">Username </label>
                    <input  type="text" name="" id="username" value={username} onChange = {(e) => handleInputChange(e)} placeholder="Username"/>
                </div>
                <div className="email">
                    <label className="form__label" htmlFor="email">Email </label>
                    <input  type="email" id="email" className="form__input" value={email} onChange = {(e) => handleInputChange(e)} placeholder="Email"/>
                </div>
                <div className="birthdate">
                    <label className="form__label" htmlFor="birthDate">Date of Birth </label>
                    {/* <DatePicker onChange = {handleInputChange(e)} value = {birthDate} /> */}
                    <DatePicker
                        onChange={onChange}
                        value={birthDate}
                        locale=	"en-EN"
                        format="dd/MM/yy"
                        maxDate={dateMinus18}
                    />
                </div>
                <div className="password">
                    <label className="form__label" htmlFor="password">Password </label>
                    <input className="form__input" type="password"  id="password" value={password} onChange = {(e) => handleInputChange(e)} placeholder="Password"/>
                </div>
                <div className="confirm-password">
                    <label className="form__label" htmlFor="confirmPassword">Confirm Password </label>
                    <input className="form__input" type="password" id="confirmPassword" value={confirmPassword} onChange = {(e) => handleInputChange(e)} placeholder="Confirm Password"/>
                </div>
            </div>
            <div class="footer">
                <button onClick={()=>handleSubmit()} type="submit" class="btn">Register</button>
            </div>
        </div>
       
    )       
}

export default RegistrationForm