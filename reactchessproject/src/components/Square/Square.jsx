import React from 'react';

import './Square.css'

export default function Square({ whiteBlack, position, image, potentialMove }) {
    let classname = "chess-piece-" + image?.slice(11, 16)
    
    if (whiteBlack % 2 == 1) {
        if (potentialMove) {
            
            
            return <div className="square white-square" style={{}}>
                    <div className="potentialMove">
                {image && <div className={classname} style={{backgroundImage: `url(${image})`, margin:'-20px'}}></div>}
                            
                            </div>
            
                        
                 </div>   
        } else {
            return <div className="square white-square" style={{}}>
                    {image && <div className={classname} style={{backgroundImage: `url(${image})`}}></div>}
               </div>   
        }     
    }
    else {
        if (potentialMove) {
            return <div className="square black-square" style={{}}>
                    <div className="potentialMove">
                            {image && <div className={classname} style={{backgroundImage: `url(${image})`, margin:'-20px'}}></div>}
                            
                            </div>
                            {/* <div className="potentialMove"></div> */}
                    </div>   
        } else {
            return <div className="square black-square">
                        {image && <div className={classname} style={{backgroundImage: `url(${image})`}}></div>}
                </div>
        }
    }

}