import React from 'react';

import './takenPieces.css'

export default function TakenPieces({takenPieces, score}) {

    console.log(takenPieces)

    console.log(score)
    return (
        <div>
            {Object.values(takenPieces).map((piece) => (
                    <img className="takenPieces" src={piece.image}></img>
            ))}
            {score > 0 ?
            <div id="score">+{score}</div> :
            <div></div> }
        </div>
        
    )


}