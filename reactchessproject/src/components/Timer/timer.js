import React, { useState, useEffect, useContext } from 'react'
import './timer.css'
import { SocketContext } from "../Context/socket";

export default function Timer({canIMove}) {
    const initialMinute = 1, initialSeconds = 10, initialMilliseconds = 100
    const [minutes, setMinutes ] = useState(initialMinute);
    const [seconds, setSeconds ] =  useState(initialSeconds);
    const [oppMinutes, setOppMinutes ] = useState(initialMinute);
    const [oppSeconds, setOppSeconds ] =  useState(initialSeconds);
    const [whoseTurn, setWhoseTurn] = useState('white')
    const socket = useContext(SocketContext)
    const [milliseconds, setMilliseconds ] =  useState(initialMilliseconds);
    console.log(canIMove)
    function startClock() {

    }

    useEffect(() => {
        socket.on('update', ({boardState, whoseTurn}) => {
            setWhoseTurn(whoseTurn)
        })
    }, []);

    useEffect(()=>{
        
        // let myInterval, oppInterval    
        // if (whoseTurn === "white" ) {
            let myInterval = setInterval(() => {
                if (whoseTurn === "white") {
                    if (milliseconds > 0) {
                        setMilliseconds(milliseconds - 100)
                    }
                    // if (milliseconds === 0) {
                    //     if (seconds > 0) {
                    //         setSeconds(seconds - 1);
                    //     }
                    // }
                    if (milliseconds === 0) {
                        if (seconds === 0) {
                            if (minutes === 0) {
                                clearInterval(myInterval)
                                
                            } else {
                                setMinutes(minutes - 1)
                                setSeconds(59);
                            }
                        } else {
                            setSeconds(seconds - 1);
                            setMilliseconds(900);
                        } 
                    }
                }

                    // if (seconds === 0) {
                    //     if (minutes === 0) {
                    //         clearInterval(myInterval)
                    //     } else {
                    //         setMinutes(minutes - 1);
                    //         setSeconds(59);
                    //     }
                    // } 
                    
                 
            }, 100)
            let oppInterval = setInterval(() => {
                if (whoseTurn === "black") {
                    if (oppSeconds > 0) {
                        setOppSeconds(oppSeconds - 1);
                    }
                    if (oppSeconds === 0) {
                        if (oppMinutes === 0) {
                            clearInterval(oppInterval)
                        } else {
                            setOppMinutes(oppMinutes - 1);
                            setOppSeconds(59);
                        }
                    } 
                }
            }, 1000)
        
        return ()=> {
            clearInterval(myInterval);
            clearInterval(oppInterval)
            };
      }, );


    return (
        // <div>
        // { minutes === 0 && seconds === 0
        //     ? null
        //     : <h1> {minutes}:{seconds < 10 ?  `0${seconds}` : seconds}</h1> 
        // }
        // </div>

        <div class="player">
      
         <div class="player__tile player-1">
           <div class="player__digits">
             <span id="min1">{minutes}</span>:<span id="sec1">{seconds < 10 ?  `0${seconds}` : seconds}</span>:<span id="ms1">{milliseconds < 100 ?  `0${milliseconds}` : milliseconds < 10 ? `00${milliseconds}` : milliseconds}</span>
           </div>
         </div>
        
         <div class="player__tile player-2">
           <div class="player__digits">
             <span id="min2">{oppMinutes}</span>:<span id="sec2">{oppSeconds < 10 ?  `0${oppSeconds}` : oppSeconds}</span>
           </div>
         </div>
        
       </div>

    

         
    )
}

// export default function Timer() {
//     const [seconds, setSeconds] = useState(null)
//     return (
//         <div class="player">
      
//         <div class="player__tile player-1">
//           <div class="player__digits">
//             <span id="min">10</span>:<span id="sec">00</span>
//           </div>
//         </div>
        
//         <div class="player__tile player-2">
//           <div class="player__digits">
//             <span id="min2">10</span>:<span id="sec2">00</span>
//           </div>
//         </div>
        
//       </div>
//     )
// }


