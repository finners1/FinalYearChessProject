const express = require("express");
const http = require("http");
const socketIo = require("socket.io");
var cors = require('cors')
const { randomUUID } = require('crypto');
const {randPiece} = require('./utilities/utils')
const Board = require('./utilities/board')


const PORT = process.env.PORT || 6001;
const index = require("./routes/index");
const { SocketAddress } = require("net");
const { move } = require("./routes/index");
const mysql = require("mysql")
const axios = require("axios")
require("dotenv").config()
const socketioJwt   = require('socketio-jwt');

const app = express();

const DB_HOST = process.env.DB_HOST
const DB_USER = process.env.DB_USER
const DB_PASSWORD = process.env.DB_PASSWORD
const DB_DATABASE = process.env.DB_DATABASE
const DB_PORT = process.env.DB_PORT



const db = mysql.createPool({
    connectionLimit: 100,
    host: DB_HOST,
    user: DB_USER,
    password: DB_PASSWORD,
    database: DB_DATABASE,
    port: DB_PORT
 })


const server = http.createServer(app);

const io = socketIo(server, {
    cors: {
        origin: "https://final-year-chess-project.vercel.app"
    }
});


var sockets = {}
var rooms = {}
let currentUser





function submitGameToDatabase(winner, loser, roomID, draw, winnerColour, loserColour) {
    db.getConnection( async (err, connection) => {

        if (err) throw (err)

        const sqlInsert = "INSERT INTO Games VALUES (?, ?, ?, ?, ?, ?)"
        const insert_query = mysql.format(sqlInsert, [roomID, winner.username, loser.username, draw, winnerColour, loserColour]);
        
        await connection.query(insert_query, (err, result) => {
            connection.release
            if (err) throw (err)
                console.log( "-----> Added game to database");
        })
    })
}

function addMoveToHistory(boardState) {
    history = [...history, boardState]
}

function submitMovesToDatabase(roomID, history, moves) {
    console.log('here')

    db.getConnection( async (err, connection) => {

        if (err) throw (err)
        console.log(moves)

        for (var h in history) {
            const sqlInsert = "INSERT INTO PreviousStates VALUES (0, ?, ?, ?)"
            const insert_query = mysql.format(sqlInsert, [JSON.stringify(history[h]), roomID, JSON.stringify(moves[h])]);
        
            await connection.query(insert_query, (err, result) => {
            connection.release
            if (err) throw (err)
                console.log( "-----> Added state to database");
        })
        }

        
    })
}
  
 


function pieceAssignment(roomID, socketID, dataID){
    const firstPiece = randPiece()
    const lastPiece = firstPiece === 'white'? 'black':'white'
    rooms[roomID][socketID].colour = firstPiece
    rooms[roomID][dataID].colour = lastPiece
}

io.use(socketioJwt.authorize({
    secret: process.env.ACCESS_TOKEN_SECRET,
    handshake: true
  }));

io.on('connection', (socket) => {
    socket.on('sendUsername', (data) => {
        console.log('----> client connected')
        sockets[socket.id] = {
            username : data.username,
            is_playing: false,
            game_id: null
        };
    })
   
    socket.on('getOpponents', (data) => {
        var response = [];
        for (var id in sockets) {
            if (id !== socket.id && !sockets[id].is_playing) {
                response.push({
                    id: id,
                    username: sockets[id].username
                });
            }
        }
        socket.emit('getOpponentsResponse', response);
            socket.broadcast.emit('newOpponentAdded', {
                id: socket.id,
                username: sockets[socket.id].username
            });
        });

    socket.on('selectOpponent', data => {
        console.log('select')
        io.to(data.id).emit('gameRequest', ({"id": socket.id}))
    })
    socket.on('gameRequestAccepted', (data) => {
        console.log('accepted')
        console.log('sockets:' + Array.from(io.sockets.sockets.keys()))
        var roomID = randomUUID();
        if (!sockets[data.id].is_playing) {
            sockets[data.id].is_playing = true;
            sockets[socket.id].is_playing = true;
            sockets[data.id].roomID = roomID;
            sockets[socket.id].roomID = roomID;
            rooms[roomID] = {
                player1: socket.id,
                player2: data.id,
                whose_turn: socket.id,
                boardState: [],
                game_status: "ongoing",
                game_winner: null
            };
            rooms[roomID][socket.id] = {
                username: sockets[socket.id].username,
                colour: ""
            }
            rooms[roomID][data.id] = {
                username: sockets[data.id].username,
                colour: ""
            };
            pieceAssignment(roomID, socket.id, data.id)
            io.sockets.sockets.get(data.id).join(roomID)
            io.sockets.sockets.get(data.id).emit('colourAssignment', rooms[roomID][data.id].colour)
            io.sockets.sockets.get(socket.id).join(roomID);
            io.sockets.sockets.get(socket.id).emit('colourAssignment', rooms[roomID][socket.id].colour)
            io.emit('excludePlayers', [socket.id]);
            io.emit('excludePlayers', [data.id]);
            io.to(roomID).emit('gameStarted', { status: true, roomID: roomID, gameData: rooms[roomID] });
        }
        
    })

    socket.on('viewProfile', () => {
        io.emit('excludePlayers', [socket.id])
    })

    socket.on('startBoardState', (data) => {
        const board = new Board(data.startBoardState)
        rooms[data.roomID].boardState = board
    })

    socket.on('getValidMoves', (piece) => {
        currentState = rooms[piece.roomID].boardState
        currentState.colour = piece.colour
        if (piece.colour === "black") {
            piece.x = Math.abs(piece.x - 7)
            piece.y = Math.abs(piece.y - 7)
        }
        let validMoves = currentState.getValidMoves(piece.x, piece.y)
        if (piece.colour === "black") {
            for (var m in validMoves) {
                validMoves[m].x = Math.abs(validMoves[m].x - 7)
                validMoves[m].y = Math.abs(validMoves[m].y - 7)
            }
            
        }
        socket.emit('returnValidMoves', (validMoves))
    })

    socket.on('move', ({move, username}) => {
         move
        currentState = rooms[move.roomID].boardState
        currentState.colour = rooms[move.roomID][socket.id].colour
        if (currentState.move(move)) {
            if (currentState.isStalemate() ) {
                
                player1 = rooms[move.roomID][rooms[move.roomID].player1]
                player2 = rooms[move.roomID][rooms[move.roomID].player2]
                currentState.switch.get(currentState.colour)
                if (!currentState.isCheck(currentState.boardState)) {
                    io.to(move.roomID).emit('addToHistory', {boardState:currentState.boardState, move:move})
                    io.to(move.roomID).emit('draw', {boardState:currentState.boardState, id: socket.id, teamColour: currentState.colour})
                    submitGameToDatabase(player1, player2, move.roomID, true, player1.colour, player2.colour)

                } else {
                    let winner = currentState.switch.get(currentState.colour)
                    io.to(move.roomID).emit('addToHistory', {boardState:currentState.boardState, move:move})
                    io.to(move.roomID).emit('winner', {boardState:currentState.boardState, teamColour: winner, method:"checkmate"})
                    
                    
                    console.log(player1.colour)
                    console.log(player2.colour)
                    if (winner === player1.colour) submitGameToDatabase(player1, player2, move.roomID, false, player1.colour, player2.colour)
                    if (winner === player2.colour) submitGameToDatabase(player2, player1, move.roomID, false, player2.colour, player1.colour)

                }
            } else {
                io.to(move.roomID).emit('update', {boardState:currentState.boardState, whoseTurn:currentState.colour})
                console.log(currentState.takenPiece)
                io.to(move.roomID).emit('addToHistory', {boardState:currentState.boardState, move:move, takenPiece:currentState.takenPiece})
                currentState.takenPiece = null
                io.to(move.roomID).emit('returnValidMoves', {})
                currentState.switchTurn()
            }
        }
        
    })
    
    socket.on('rematchRequest', (data) => {
        console.log('rematchreq')
        currentRoom = rooms[data.roomID]
        var newRoomID = randomUUID();
        rooms[newRoomID] = currentRoom
        const board = new Board(data.startBoardState)
        currentRoom.boardState = board
        io.sockets.sockets.get(rooms[newRoomID].player1).join(newRoomID)
        io.sockets.sockets.get(rooms[newRoomID].player2).join(newRoomID)
        io.to(newRoomID).emit('restart', { status: true, roomID: newRoomID, gameData: rooms[newRoomID] })
    })

    socket.on('submitHistory', ({roomID, history, moves}) => {
        submitMovesToDatabase(roomID, history, moves)
    })

    socket.on('resign', ({roomID, colour}) => {
        colour === "white" ? winner = "black" : winner = "white"
        player1 = rooms[roomID][rooms[roomID].player1]
        player2 = rooms[roomID][rooms[roomID].player2]


        if (winner === player1.colour) submitGameToDatabase(player1, player2, roomID, false, player1.colour, player2.colour)
        if (winner === player2.colour) submitGameToDatabase(player2, player1, roomID, false, player2.colour, player1.colour)

        currentState = rooms[roomID].boardState

        io.to(roomID).emit('winner', {boardState:currentState.boardState, teamColour: winner, method: "resignation"})
    })

    socket.on('offerDraw', ({roomID, colour}) => {
        socket.broadcast.to(roomID).emit('drawOffered', {colour:colour})
    })

    socket.on('acceptDraw', ({roomID}) => {
        player1 = rooms[roomID][rooms[roomID].player1]
        player2 = rooms[roomID][rooms[roomID].player2]
        currentState = rooms[roomID].boardState
        submitGameToDatabase(player1, player2, roomID, true, player1.colour, player2.colour)
        
        io.to(roomID).emit('draw', {boardState:currentState.boardState})
    })

    socket.on('leaveGame', ({roomID}) => {
        let socket1 = rooms[roomID].player1
        let socket2 = rooms[roomID].player2
        console.log('client disconnected')
        if (typeof sockets[socket.id] != "undefined") {
            if (sockets[socket.id].is_playing) {
            
                socket.broadcast.to(roomID).emit('opponentLeft', {})
                io.sockets.sockets.get(socket.id == rooms[sockets[socket.id].roomID].player1 ? rooms[sockets[socket.id].roomID].player2 : rooms[sockets[socket.id].roomID].player1).leave(sockets[socket.id].roomID);
                io.sockets.sockets.get(rooms[roomID].player1).leave(roomID)
                io.sockets.sockets.get(rooms[roomID].player2).leave(roomID)
                delete rooms[roomID];
            }
        }
        delete sockets[socket1]
        delete sockets[socket2]
        // should this be here???
        
    })
            
    socket.on('disconnect', () => {
        console.log('client disconnected')
        if (typeof sockets[socket.id] != "undefined") {
            if (sockets[socket.id].is_playing) {
                io.to(sockets[socket.id].roomID).emit('opponentLeft', {});
                io.sockets.sockets.get(socket.id == rooms[sockets[socket.id].roomID].player1 ? rooms[sockets[socket.id].roomID].player2 : rooms[sockets[socket.id].roomID].player1).leave(sockets[socket.id].roomID);
                delete rooms[sockets[socket.id].roomID];
            }
        }
        delete sockets[socket.id];
        socket.broadcast.emit('opponentDisconnected', {
            id: socket.id
        })
    })
        
    
});


server.listen(PORT, () => console.log(`Server listening on port ${PORT}`));
