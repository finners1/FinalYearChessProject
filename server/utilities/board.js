class Board {

    constructor(startBoardState) {
        this.boardState = startBoardState
        this.turn = 'white'
        this.switch = new Map([['white', 'black'], ['black', 'white']])
        this.colour = null
        this.takenPiece = []
        this.isEnPassantPossible = false
        this.hasWhiteKingMoved = false
        this.hasBlackKingMoved = false
        this.hasWhiteARookMoved = false
        this.hasBlackARookMoved = false
        this.hasWhiteHRookMoved = false
        this.hasBlackHRookMoved = false
        this.tempBoardState = []
    }

   

    move(move) {
        const step = this.colour === "white" ? 1 : -1
   
        switch(move.piece) {
            case 1:
                if (this.isValidPawnMove(move)) {
                    this.isPromotion(move)
                    this.movePiece(move)
                    return true
                    } else return false
            case 2:
                if (this.isValidKnightMove(move)) {
                    this.movePiece(move)
                    return true
                } else {
                    return false
                }
            case 3:
                if (this.isValidBishopMove(move)) {
                    this.movePiece(move)
                    return true
                } else {
                    return false
                }
            case 5:
                if (this.isValidRookMove(move)) {
                    this.movePiece(move)
                    
                    if (move.currentX === 0) {
                        this.colour === "white" ? this.hasWhiteARookMoved = true : this.hasBlackARookMoved = true
                    }
                    if (move.currentX === 7) {
                        this.colour === "white" ? this.hasWhiteHRookMoved = true : this.hasBlackHRookMoved = true
                    }
                    return true
                } else {
                    return false
                }
            case 9:
                if (this.isValidQueenMove(move)) {
                    this.movePiece(move)
                    return true
                } else {
                    return false
                }
            
            case 10:
                if (this.isValidKingMove(move)) {
                    this.movePiece(move)
                    this.colour === "white" ? this.hasWhiteKingMoved = true : this.hasBlackKingMoved = true
                    this.hasMyKingMoved = true
                    return true
                }
                else {
                    return false
                }
        }

    }

    blockChecks(move) {
        let boardStateCopy = this.boardState
        if (this.isCheck(boardStateCopy)) {
            console.log('is check')
            if (this.doesMoveBlockCheck(move)) {
                console.log('blocks check')
                return true
            } else {
                return false
            }
        }
        return true
    }

    calculateAbs(value) {
        value = Math.abs(value - 7)
    }

    

    doesMoveBlockCheck(move) {
        let rtrnTrue = false
        const boardStateCopy = this.boardState
        this.tempBoardState = boardStateCopy

        let pieceToTake = this.isPieceTakenOnTempArray(move.targetX, move.targetY)

 
        if (pieceToTake) {this.takeEnemyPieceOnTempArray(move) }
        this.movePieceOnTempArray(move)

        if (!this.isCheck(this.tempBoardState)) {
            rtrnTrue = true
        } 
        this.movePieceBackOnTempArray(move) 

        
        if (pieceToTake) {this.returnTakenPieceToTempArray(pieceToTake) } 
        
        return rtrnTrue
    }

    doesMoveBlockOpponentCheck(move) {
        let rtrnTrue = false
        const boardStateCopy = this.boardState
        this.tempBoardState = boardStateCopy
        this.movePieceOnTempArray(move)
        if (!this.isOpponentInCheck(this.tempBoardState)) {
            rtrnTrue = true
        } 
        this.movePieceBackOnTempArray(move) 
        
        return rtrnTrue
    }

    doesEnPassantBlockCheck(move) {
        
        let rtrnTrue = false
        const boardStateCopy = this.boardState
        this.tempBoardState = boardStateCopy
        this.movePieceOnTempArray(move)
        this.takeEnemyPieceOnTempArrayEnPassant(move)
        if (!this.isCheck(this.tempBoardState)) {
            rtrnTrue = true
        } 
        this.movePieceBackOnTempArray(move) 
        
        return rtrnTrue
    }

    takeEnemyPieceOnTempArrayEnPassant(move) {
        const step = this.colour === "white" ? 1 : -1
        const boardStateCopy = this.boardState
        this.tempBoardState = boardStateCopy
        let pieceToTake = this.tempBoardState.find((p) => p.x === move.targetX && p.y === move.targetY - step)
        var newState = this.tempBoardState.filter((piece) => piece != pieceToTake)
        this.tempBoardState = newState
    }

    takeEnemyPieceOnTempArray(move) {
        const boardStateCopy = this.boardState
        this.tempBoardState = boardStateCopy
        let pieceToTake = this.tempBoardState.find((p) => p.x === move.targetX && p.y === move.targetY)
        var newState = this.tempBoardState.filter((piece) => piece != pieceToTake)
        this.tempBoardState = newState
            
    }

    doesMoveRevealCheck(move) {
        let rtrnTrue = false
        const boardStateCopy = this.boardState
        this.tempBoardState = boardStateCopy
        let pieceToTake = this.isPieceTakenOnTempArray(move.targetX, move.targetY)
        if (pieceToTake) {this.takeEnemyPieceOnTempArray(move) }
        this.movePieceOnTempArray(move)
        if (this.isCheck(this.tempBoardState)) {
            rtrnTrue = true
        } 
        this.movePieceBackOnTempArray(move) 
        if (pieceToTake) {this.returnTakenPieceToTempArray(pieceToTake) } 
        return rtrnTrue
    }

    movePieceOnTempArray(move) {
        for (var i = 0; i < this.tempBoardState.length; i++) {
            if (this.tempBoardState[i].x == move.currentX && this.tempBoardState[i].y == move.currentY) {
                this.tempBoardState[i].x = move.targetX
                this.tempBoardState[i].y = move.targetY
            }
        }
    }

    isPieceTakenOnTempArray(x, y) {
        let pieceTaken = this.tempBoardState.find((p) => p.x === x && p.y === y)
        return pieceTaken
    }

    movePieceBackOnTempArray(move) {
        for (var i = 0; i < this.tempBoardState.length; i++) {
            if (this.tempBoardState[i].x == move.targetX && this.tempBoardState[i].y == move.targetY) {
                this.tempBoardState[i].x = move.currentX
                this.tempBoardState[i].y = move.currentY
            }
        }
    }

    returnTakenPieceToTempArray(piece) {
        for (var p in this.tempBoardState) {
            if (this.tempBoardState[p].x === piece.x && this.tempBoardState[p].y === piece.y) {
                this.tempBoardState[p] = piece
            }
        }
    }

    movePiece(move) {
        const enPassantRow = this.colour === "white" ? 1 : 6
        const step = this.colour === "white" ? 1 : -1
        for (var i = 0; i < this.boardState.length; i++) {
            if (this.boardState[i].x == move.currentX && this.boardState[i].y == move.currentY) {
            
                if (this.boardState[i].pieceType === 1 && move.targetY === move.currentY + 2 * step && move.currentY === enPassantRow) {
                    this.boardState[i].isEnPassantPossible = true
                } else {
                    this.boardState[i].isEnPassantPossible = false
                }
                this.boardState[i].x = move.targetX
                this.boardState[i].y = move.targetY
            }
            else {
                this.boardState[i].isEnPassantPossible = false
            }
        }
        

        
    }

    isSquareAttacked(x, y, boardState) {
        let validRookMoves = this.getValidRookMoves(x, y)
        let validBishopMoves = this.getValidBishopMoves(x, y)
        let validKnightMoves = this.getValidKnightMoves(x, y)
        let step
        for (var i in validRookMoves) {
            for (var p in boardState) {
                
                if (boardState[p].x === validRookMoves[i].x && boardState[p].y === validRookMoves[i].y && boardState[p].pieceType === 5 && boardState[p].teamType !== this.colour) {
                    return true
                }
                if (boardState[p].x === validRookMoves[i].x && boardState[p].y === validRookMoves[i].y && boardState[p].pieceType === 9 && boardState[p].teamType !== this.colour) {
                    return true
                }
            }
            
        }

        for (var i in validBishopMoves) {
            for (var p in boardState) {
                if (boardState[p].x === validBishopMoves[i].x && boardState[p].y === validBishopMoves[i].y && boardState[p].pieceType === 3 && boardState[p].teamType !== this.colour) {
                    return true
                }
                if (boardState[p].x === validBishopMoves[i].x && boardState[p].y === validBishopMoves[i].y && boardState[p].pieceType === 9 && boardState[p].teamType !== this.colour) {
                    return true
                }
            }
            
        }

        for (var i in validKnightMoves) {
            for (var p in boardState) {
                if (boardState[p].x === validKnightMoves[i].x && boardState[p].y === validKnightMoves[i].y && boardState[p].pieceType === 2 && boardState[p].teamType !== this.colour) {
                    return true
                }
            }
        }

        this.colour === "white" ? step = 1 : step = -1

        for (var p in boardState) {
            if (boardState[p].x === x + 1 && boardState[p].y === y + step && boardState[p].pieceType === 1 && boardState[p].teamType !== this.colour) {
                return true
            }
        }
        for (var p in boardState) {
            if (boardState[p].x === x - 1 && boardState[p].y === y + step && boardState[p].pieceType === 1 && boardState[p].teamType !== this.colour) {
                return true
            }
        }
        return false
    }

    doesSquareContainMyPiece(x, y) {
        const piece = this.boardState.find((p) => p.x === x && p.y === y && p.teamType === this.colour);
        if (piece) {
            return true
        } else {
            return false
        }
    }

    whichEnemyPieceDoesSquareContain(x, y) {
        const piece = this.boardState.find((p) => p.x === x && p.y === y && p.teamType !== this.colour);
        if (piece) {
            return piece
        } else {
            return false
        }
    }

    doesSquareContainEnemyPiece(x, y) {
        const piece = this.boardState.find((p) => p.x === x && p.y === y && p.teamType !== this.colour);
        if (piece) {
            return true
        } else {
            return false
        }
    }

    doesSquareContainPiece(x, y) {
        const piece = this.boardState.find((p) => p.x === x && p.y === y);
        if (piece) {
            return true
        } else {
            return false
        }
    }

    takeEnemyPiece(move) {
        let pieceToTake = this.boardState.find((p) => p.x === move.targetX && p.y === move.targetY && p.colour !== this.colour)
        var newState = this.boardState.filter((piece) => piece != pieceToTake)
        this.boardState = newState
        this.takenPiece = pieceToTake
    }
   
    

    getValidPawnMoves(x, y) {
        let step, pawnRow
        let validPawnMoves = []

        this.colour === "white" ? pawnRow = 1 : pawnRow = 6
        this.colour === "white" ? step = 1 : step = -1

        if (!this.doesSquareContainPiece( x, y + step) ) {
            validPawnMoves.push({x: x, y: y + step})
        }
        if (y === pawnRow) {
            if (!this.doesSquareContainPiece( x, y + 2 * step) ) {
                if (!this.doesSquareContainPiece( x, y + 1 * step) ) {
                    validPawnMoves.push({x: x, y: y + 2 * step})
                }
            }
        }
        return validPawnMoves
    }

    isEnPassant(move) {
        let step, pawnRow
        this.colour === "white" ? pawnRow = 4 : pawnRow = 3
        this.colour === "white" ? step = 1 : step = -1
        if (move.currentY === pawnRow) {
            if (move.targetY === move.currentY + step && move.targetX === move.currentX + 1){
                const piece = this.whichEnemyPieceDoesSquareContain( move.currentX + 1, move.currentY) 
                if (piece.pieceType === 1 && piece.isEnPassantPossible === true) {
                    return true
                }
            }
            if (move.targetY === move.currentY + step && move.targetX === move.currentX - 1){
                const piece = this.whichEnemyPieceDoesSquareContain( move.currentX - 1, move.currentY) 
                if (piece.pieceType === 1 && piece.isEnPassantPossible === true) {
                    return true
                }
            }
        }
        return false
    }

    getMyValidEnPassantMoves(x, y) {
        let myValidEnPassantMoves = []
        let pawnRow = this.colour === "white" ? 4 : 3
        let step = this.colour === "white" ? 1 : -1
        if (y === pawnRow) {
            const rightPiece = this.whichEnemyPieceDoesSquareContain(x + 1, y)
            if (rightPiece.pieceType === 1 && rightPiece.isEnPassantPossible === true) {
                let move = {currentX: x, currentY: y, targetX: x + 1, targetY: y + step}
                if (!this.doesMoveRevealCheck(move)) {
                    myValidEnPassantMoves.push({x: x + 1, y: y + step})
                }
            }
            const leftPiece = this.whichEnemyPieceDoesSquareContain(x - 1, y)
            if (leftPiece.pieceType === 1 && leftPiece.isEnPassantPossible === true) {
                let move = {currentX: x, currentY: y, targetX: x - 1, targetY: y + step}
                if (!this.doesMoveRevealCheck(move)) {
                    myValidEnPassantMoves.push({x: x - 1, y: y + step})
                }
            }
        }
        return myValidEnPassantMoves
    }



    getValidPawnAttackingMoves(x, y) {
        let step
        this.colour === "white" ? step = 1 : step = -1
        let validPawnAttackingMoves = []
        if (this.doesSquareContainEnemyPiece(x - 1, y + step)) {
            validPawnAttackingMoves.push({x: x - 1, y: y + step})
        }
        if (this.doesSquareContainEnemyPiece(x + 1, y + step)) {
            validPawnAttackingMoves.push({x: x + 1, y: y + step})
        }
        return validPawnAttackingMoves
    }

    isValidPawnMove(move) {
        const step = this.colour === "white" ? 1 : -1
        let validPawnMoves = this.getMyValidPawnMoves(move.currentX, move.currentY)
        let validPawnAttackingMoves = this.getMyValidPawnAttackingMoves(move.currentX, move.currentY)
        let validEnPassantMoves = this.getMyValidEnPassantMoves(move.currentX, move.currentY)
        if (this.turn === this.colour) {
            for (let i = 0; i < validPawnMoves.length; i++) {
                if (move.targetX === validPawnMoves[i].x && move.targetY === validPawnMoves[i].y) {
                    return true
                }
            }
            for (let i = 0; i < validPawnAttackingMoves.length; i++) {
                if (move.targetX === validPawnAttackingMoves[i].x && move.targetY === validPawnAttackingMoves[i].y) {
                    
                    if (this.doesSquareContainEnemyPiece(move.targetX, move.targetY)) {
                        this.takeEnemyPiece(move)
                        return true
                    }
                }
            }
            for (var i in validEnPassantMoves) {
                if (move.targetX === validEnPassantMoves[i].x && move.targetY === validEnPassantMoves[i].y) {
                    let moveEnPassant = {currentX: move.currentX, currentY: move.currentY , targetX: move.targetX, targetY: move.targetY - step}
                    this.takeEnemyPiece(moveEnPassant)
                    return true
                }
            }   

        }        
    }

    getMyValidPawnMoves(x, y) {
        let myValidPawnMoves = []
        let validPawnMoves = this.getValidPawnMoves(x, y)
        for (var m in validPawnMoves) {
            let move = {currentX: x, currentY: y, targetX: validPawnMoves[m].x, targetY: validPawnMoves[m].y}
            if (!this.doesMoveRevealCheck(move)) {
                if (this.blockChecks(move)) {
                    myValidPawnMoves.push(validPawnMoves[m])
                }
            }
        }
        return myValidPawnMoves
    }

    getMyValidPawnAttackingMoves(x, y) {
        let myValidPawnAttackingMoves = []
        let validPawnAttackingMoves = this.getValidPawnAttackingMoves(x, y)
        for (var m in validPawnAttackingMoves) {
            let move = {currentX: x, currentY: y, targetX: validPawnAttackingMoves[m].x, targetY: validPawnAttackingMoves[m].y}
            if (!this.doesMoveRevealCheck(move)) {
                if (this.blockChecks(move)) {
                    myValidPawnAttackingMoves.push(validPawnAttackingMoves[m])
                }
            }
        }
        return myValidPawnAttackingMoves
    }

    getMyValidKnightMoves(x, y) {
        let myValidKnightMoves = []
        let validKnightMoves = this.getValidKnightMoves(x, y)
        for (var m in validKnightMoves) {
            let move = {currentX: x, currentY: y, targetX: validKnightMoves[m].x, targetY: validKnightMoves[m].y}
            if (!this.doesMoveRevealCheck(move)) {
                if (this.blockChecks(move)) {
                    myValidKnightMoves.push(validKnightMoves[m])
                }
            }
        }
        return myValidKnightMoves
    }

    getMyValidBishopMoves(x, y) {
        let myValidBishopMoves = []
        let validBishopMoves = this.getValidBishopMoves(x, y)
        for (var m in validBishopMoves) {
            let move = {currentX: x, currentY: y, targetX: validBishopMoves[m].x, targetY: validBishopMoves[m].y}
            if (!this.doesMoveRevealCheck(move)) {
                if (this.blockChecks(move)) {
                    myValidBishopMoves.push(validBishopMoves[m])
                }
            }
        }
        console.log(myValidBishopMoves)
        return myValidBishopMoves
    }

    getMyValidRookMoves(x, y) {
        let myValidRookMoves = []
        let validRookMoves = this.getValidRookMoves(x, y)
        for (var m in validRookMoves) {
            let move = {currentX: x, currentY: y, targetX: validRookMoves[m].x, targetY: validRookMoves[m].y}
            if (!this.doesMoveRevealCheck(move)) {
                if (this.blockChecks(move)) {
                    myValidRookMoves.push(validRookMoves[m])
                }
            }
        }
        return myValidRookMoves
    }

    getValidKnightMoves(x, y) {
        let validKnightMoves = []
        if (!this.doesSquareContainMyPiece( x + 1, y + 2, this.colour) ) {
                if ( x + 1 <= 7 && y + 2 <= 7) {
                    validKnightMoves.push({x: x + 1, y: y + 2})
                }
        }
        if (!this.doesSquareContainMyPiece( x - 1, y + 2, this.colour) ) {
                if ( x - 1 >= 0 && y + 2 <= 7) {
                    validKnightMoves.push({x: x - 1, y: y + 2})
                }
        }
        if (!this.doesSquareContainMyPiece( x + 2, y + 1, this.colour) ) {
                if ( x + 2 <= 7 && y + 1 <= 7) {
                    validKnightMoves.push({x: x + 2, y: y + 1})
                }
        }
        if (!this.doesSquareContainMyPiece( x + 2, y - 1, this.colour) ) {
                if ( x + 2 <= 7 && y - 1 >= 0) {
                    validKnightMoves.push({x: x + 2, y: y - 1})
                }
        }
        if (!this.doesSquareContainMyPiece( x - 2, y + 1, this.colour) ) {
            if ( x - 2 >= 0 && y + 1 <= 7) {
                validKnightMoves.push({x: x - 2, y: y + 1})
            }
        }
        if (!this.doesSquareContainMyPiece( x - 2, y - 1, this.colour) ) {
                if ( x - 2 >= 0 && y - 1 >= 0) {
                    validKnightMoves.push({x: x - 2, y: y - 1})
                }
        }
        if (!this.doesSquareContainMyPiece( x + 1, y - 2, this.colour) ) {
                if ( x + 1 <= 7 && y - 2 >= 0) {
                    validKnightMoves.push({x: x + 1, y: y - 2})
                }
        }
        if (!this.doesSquareContainMyPiece( x - 1, y - 2, this.colour) ) {
                if ( x - 1 >= 0 && y - 2 >= 0) {
                    validKnightMoves.push({x: x - 1, y: y - 2})
                }
        }
        return validKnightMoves
    }

    isValidKnightMove(move) {
        let validKnightMoves = this.getMyValidKnightMoves(move.currentX, move.currentY)
        // console.log('kngiht moves: ' + validKnightMoves[0].x + validKnightMoves[0].y + validKnightMoves[1].x + validKnightMoves[1].y)
        if (this.turn === this.colour) {
            for (let i = 0; i < validKnightMoves.length; i++) {
                if (move.targetX === validKnightMoves[i].x && move.targetY === validKnightMoves[i].y) {
                    if (this.doesSquareContainEnemyPiece(move.targetX, move.targetY, this.colour)) {
                        this.takeEnemyPiece(move)
                        return true
                    }
                    return true
                }
            }
        }
    }

    getValidBishopMoves(x, y) {
        let validBishopMoves = []
        for (let i = 1; i < 8; i++) {
            if (this.doesSquareContainMyPiece( x + i, y + i, this.colour) ) {
                break
            }
            if (this.doesSquareContainEnemyPiece(x + i - 1, y + i - 1, this.colour)) {
                break
            }
            if (x + i > 7 || y + i > 7) {
                break
            }
            validBishopMoves.push({x: x + i , y: y + i })
        }
        for (let i = 1; i < 8; i++) {
            if (this.doesSquareContainMyPiece( x + i, y - i, this.colour) ) {
                break
            }
            if (this.doesSquareContainEnemyPiece(x + i - 1, y - i + 1, this.colour)) {
                break
            }
            if (x + i > 7 || y - i < 0) {
                break
            }
            validBishopMoves.push({x: x + i , y: y - i })
        }
        for (let i = 1; i < 8; i++) {
            if (this.doesSquareContainMyPiece( x - i, y + i, this.colour) ) {
                break
            }
            if (this.doesSquareContainEnemyPiece(x - i + 1, y + i - 1, this.colour)) {
                break
            }
            if (x - i < 0 || y + i > 7) {
                break
            }
            validBishopMoves.push({x: x - i , y: y + i })
        }
        for (let i = 1; i < 8; i++) {
            if (this.doesSquareContainMyPiece( x - i, y - i, this.colour) ) {
                break
            }
            if (this.doesSquareContainEnemyPiece(x - i + 1, y - i + 1, this.colour)) {
                break
            }
            if (x - i < 0 || y - i < 0) {
                break
            }
            validBishopMoves.push({x: x - i , y: y - i })
        }
        return validBishopMoves
    }

    getValidRookMoves(x, y) {
        let validRookMoves = []
        for (let i = 1; i < 8; i++) {
            if (this.doesSquareContainMyPiece( x, y + i, this.colour) ) {
                break
            }
            if (this.doesSquareContainEnemyPiece(x, y + i - 1, this.colour)) {
                break
            }
            if (y + i > 7) {
                break
            }
            validRookMoves.push({x: x , y: y + i })
        }
        for (let i = 1; i < 8; i++) {
            if (this.doesSquareContainMyPiece( x, y - i, this.colour)) {
                break
            }
            if (this.doesSquareContainEnemyPiece(x, y - i + 1, this.colour)) {
                break
            }
            if (y - i < 0) {
                break
            }
            validRookMoves.push({x: x , y: y - i })
        }
        for (let i = 1; i < 8; i++) {
            if (this.doesSquareContainMyPiece( x + i, y, this.colour)) {
                break
            }
            if (this.doesSquareContainEnemyPiece(x + i - 1, y, this.colour)) {
                break
            }
            if (x + i > 7) {
                break
            }
            validRookMoves.push({x: x + i , y: y })
        }
        for (let i = 1; i < 8; i++) {
            if (this.doesSquareContainMyPiece( x - i, y, this.colour)) {
                break
            }
            if (this.doesSquareContainEnemyPiece(x - i + 1, y, this.colour)) {
                break
            }
            if (x - i < 0) {
                break
            }
            validRookMoves.push({x: x - i , y: y })
        }
        return validRookMoves
    }

    isValidRookMove(move) {
        let validRookMoves = this.getMyValidRookMoves(move.currentX, move.currentY)
        if (!this.doesSquareContainMyPiece(move.targetX, move.targetY, this.colour)) {
            if (this.turn === this.colour) {
                for (let i = 0; i < validRookMoves.length; i++) {
                    if (move.targetX === validRookMoves[i].x && move.targetY === validRookMoves[i].y) {
                        if (this.doesSquareContainEnemyPiece(move.targetX, move.targetY, this.colour)) {
                            this.takeEnemyPiece(move)
                            return true
                        }
                        return true
                    }
                }
            }
        }
    }

    isValidBishopMove(move) {
        let squaresToTravel = []
        let validBishopMoves = this.getMyValidBishopMoves(move.currentX, move.currentY)
        if (this.turn === this.colour) {
            for (let i = 0; i < validBishopMoves.length; i++) {
                if (move.targetX === validBishopMoves[i].x && move.targetY === validBishopMoves[i].y) {
                    if (this.doesSquareContainEnemyPiece(move.targetX, move.targetY, this.colour)) {
                        this.takeEnemyPiece(move)
                        return true
                    }
                    return true
                }
            }
        }
    }

    getValidKingMoves(x, y) {
        let validKingMoves = []
        if (!this.doesSquareContainMyPiece(x + 1, y)) {
            if (!this.isSquareAttacked(x + 1, y, this.boardState)) {
                if (x + 1 <= 7) {
                    validKingMoves.push({x: x + 1, y: y})
                }
            }
        }
        if (!this.doesSquareContainMyPiece(x - 1, y)) {
            if (!this.isSquareAttacked(x - 1, y, this.boardState)) {
                if (x - 1 >= 0) {
                    validKingMoves.push({x: x - 1, y: y})
                }
            }
        }
        if (!this.doesSquareContainMyPiece(x, y + 1)) {
            if (!this.isSquareAttacked(x, y + 1, this.boardState)) {
                if (y + 1 < 7) {
                    validKingMoves.push({x: x, y: y + 1})
                }
            }
        }
        if (!this.doesSquareContainMyPiece(x, y - 1)) {
            if (!this.isSquareAttacked(x, y - 1, this.boardState)) {
                if (y - 1 >= 0) {
                    validKingMoves.push({x: x, y: y - 1})
                }
            }
        }
        if (!this.doesSquareContainMyPiece(x + 1, y + 1)) {
            if (!this.isSquareAttacked(x + 1, y + 1, this.boardState)) {
                if (y + 1 <= 7 && x + 1 <= 7) {
                    validKingMoves.push({x: x + 1, y: y + 1})
                }
            }
        }
        if (!this.doesSquareContainMyPiece(x + 1, y - 1)) {
            if (!this.isSquareAttacked(x + 1, y - 1, this.boardState)) {
                if (y - 1 >= 0 && x + 1 <= 7) {
                    validKingMoves.push({x: x + 1, y: y - 1})
                }
            }
        }
        if (!this.doesSquareContainMyPiece(x - 1, y + 1)) {
            if (!this.isSquareAttacked(x - 1, y + 1, this.boardState)) {
                if (x - 1 >= 0 && y + 1 <= 7) {
                    validKingMoves.push({x: x - 1, y: y + 1})
                }
            }
        }
        if (!this.doesSquareContainMyPiece(x - 1, y - 1)) {
            if (!this.isSquareAttacked(x - 1, y - 1, this.boardState)) {
                if (x - 1 >= 0 && y - 1 >= 0) {
                    validKingMoves.push({x: x - 1, y: y - 1})
                }
            }
        }
        return validKingMoves
    }

    getValidCastleMoves(x, y) {
        let castleRow
        let validCastleMoves = []
        this.colour === "white" ? castleRow = 0 : castleRow = 7
        let hasARookMoved = this.colour === "white" ? (this.hasWhiteARookMoved) : (this.hasBlackARookMoved)
        let hasHRookMoved = this.colour === "white" ? (this.hasWhiteHRookMoved) : (this.hasBlackHRookMoved)
        let hasMyKingMoved = this.colour === "white" ? (this.hasWhiteKingMoved) : (this.hasBlackKingMoved)
        if (!hasMyKingMoved) {
            if (!hasARookMoved) {
                if (!this.doesSquareContainPiece(1, castleRow) && !this.doesSquareContainPiece(2, castleRow) && !this.doesSquareContainPiece(3, castleRow)) {
                    if (!this.isSquareAttacked(1, castleRow, this.boardState) && !this.isSquareAttacked(2, castleRow, this.boardState) && !this.isSquareAttacked(3, castleRow, this.boardState) && !this.isSquareAttacked(4, castleRow, this.boardState)) {
                        validCastleMoves.push({x: x - 2, y: y})
                    }
                }
            }
            if (!hasHRookMoved) {
                if (!this.doesSquareContainPiece(6, castleRow) && !this.doesSquareContainPiece(5, castleRow)) {
                    if (!this.isSquareAttacked(6, castleRow, this.boardState) && !this.isSquareAttacked(5, castleRow, this.boardState) && !this.isSquareAttacked(3, castleRow, this.boardState) && !this.isSquareAttacked(4, castleRow, this.boardState)) {
                        validCastleMoves.push({x: x + 2, y: y})
                    }
                }
            }
        }
        return validCastleMoves
    }

    isValidKingMove(move) {
        let squaresToTravel = []
        let castleRow
        this.colour === "white" ? castleRow = 0 : castleRow = 7
        let validKingMoves = this.getValidKingMoves(move.currentX, move.currentY)
        let validCastleMoves = this.getValidCastleMoves(move.currentX, move.currentY)
        if (this.turn === this.colour) {
            for (let i = 0; i < validKingMoves.length; i++) {
                if (move.targetX === validKingMoves[i].x && move.targetY === validKingMoves[i].y) {
                    if (this.doesSquareContainEnemyPiece(move.targetX, move.targetY, this.colour)) {
                        this.takeEnemyPiece(move)
                        return true
                    }
                    return true
                }
            }
            for (let i  = 0; i < validCastleMoves.length; i++ ) {
                if (move.targetX === validCastleMoves[i].x && move.targetY === validCastleMoves[i].y) {
                    if (move.targetX === 2) {
                        let rookMove = {currentX: 0, currentY: castleRow, targetX: 3, targetY: castleRow, piece: 5, team: this.colour, roomID: this.roomID, boardState: this.boardState} 
                        this.movePiece(rookMove) 
                        return true
                    }
                    if (move.targetX === 6) {
                        let rookMove = {currentX: 7, currentY: castleRow, targetX: 5, targetY: castleRow, piece: 5, team: this.colour, roomID: this.roomID, boardState: this.boardState} 
                        this.movePiece(rookMove) 
                        return true
                    }
                }
            }
        }
    }
    

    getValidMoves(x, y) {
        
        let piece = this.boardState.find((p) => p.x === x && p.y === y && p.teamType === this.colour)
        if (piece) {
            switch(piece.pieceType) {
                case 1:
                    
                    return this.getMyValidPawnMoves(x, y).concat(this.getMyValidPawnAttackingMoves(x, y), this.getMyValidEnPassantMoves(x, y))
                case 2:
                    
                    return this.getMyValidKnightMoves(x, y)
                case 3:
                    return this.getMyValidBishopMoves(x, y)
                case 5:
                    return this.getMyValidRookMoves(x, y)
                case 9:
                    return this.getMyValidBishopMoves(x, y).concat(this.getMyValidRookMoves(x,y))
                case 10:
                    return this.getValidKingMoves(x, y).concat(this.getValidCastleMoves(x,y))
            }
        }
        
    }
    
    

    isValidQueenMove(move) {
        if (this.isValidBishopMove(move) || this.isValidRookMove(move)) { 
            return true
        }
        return false
    }

    isCheck(boardState) {
        let rtrnTrue = false
        for (var p in boardState) {
            if (boardState[p].pieceType === 10 && boardState[p].teamType === this.colour) {
                if (this.isSquareAttacked(boardState[p].x, boardState[p].y, boardState)) {
                    return true
                }
            }
        }
        return false
    }

    isPromotion(move) {
        let promotionRow
        this.colour === "white" ? promotionRow = 7 : promotionRow = 0
        if (move.targetY === promotionRow) {
            this.boardState.forEach((p) => {
                if (p.x === move.currentX && p.y === move.currentY) {
                    p.image = "assets/img/" + this.colour + "Queen.png"
                    p.pieceType = 9
                }
            });
            return true
        }
    }



    isStalemate() {
        let validPawnMoves, validPawnAttackingMoves, validBishopMoves, validKnightMoves, validRookMoves, validKingMoves
        let newMove
        let colourSwitch = new Map([['white', 'black'], ['black', 'white']])
        
        this.colour = colourSwitch.get(this.colour)
        for (var p in this.boardState) {
            if (this.boardState[p].teamType === this.colour && this.boardState[p].pieceType === 1) {
                validPawnMoves = this.getValidPawnMoves(this.boardState[p].x, this.boardState[p].y)
                for (let i = 0; i < validPawnMoves.length; i++) {
                    newMove = {currentX: this.boardState[p].x, currentY: this.boardState[p].y, targetX:  validPawnMoves[i].x, targetY: validPawnMoves[i].y}
                    if (this.doesMoveBlockCheck(newMove)) {
                        
                        return false
                    }
                }
            
                validPawnAttackingMoves = this.getValidPawnAttackingMoves(this.boardState[p].x, this.boardState[p].y)
                for (let i = 0; i < validPawnAttackingMoves.length; i++) {
                    newMove = {currentX: this.boardState[p].x, currentY: this.boardState[p].y, targetX:  validPawnAttackingMoves[i].x, targetY: validPawnAttackingMoves[i].y}
                    if (this.doesMoveBlockCheck(newMove)) {
                        
                        return false
                    }
                }
                
            }
            if (this.boardState[p].teamType === this.colour && this.boardState[p].pieceType === 2) {
                validKnightMoves = this.getValidKnightMoves(this.boardState[p].x, this.boardState[p].y)
                for (let i = 0; i < validKnightMoves.length; i++) {
                    newMove = {currentX: this.boardState[p].x, currentY: this.boardState[p].y, targetX:  validKnightMoves[i].x, targetY: validKnightMoves[i].y}
                    if (this.doesMoveBlockCheck(newMove)) {
                        
                        return false
                    }
                }
            }
            if (this.boardState[p].teamType === this.colour && (this.boardState[p].pieceType === 3 || this.boardState[p].pieceType === 9)) {
                validBishopMoves = this.getValidBishopMoves(this.boardState[p].x, this.boardState[p].y)
                for (let i = 0; i < validBishopMoves.length; i++) {
                    newMove = {currentX: this.boardState[p].x, currentY: this.boardState[p].y, targetX:  validBishopMoves[i].x, targetY: validBishopMoves[i].y}
                    if (this.doesMoveBlockCheck(newMove)) {
                        
                        return false
                    }
                }
            }
            if (this.boardState[p].teamType === this.colour && (this.boardState[p].pieceType === 5 || this.boardState[p].pieceType === 9)) {
                validRookMoves = this.getValidRookMoves(this.boardState[p].x, this.boardState[p].y)
                for (let i = 0; i < validRookMoves.length; i++) {
                    newMove = {currentX: this.boardState[p].x, currentY: this.boardState[p].y, targetX:  validRookMoves[i].x, targetY: validRookMoves[i].y}
                    if (this.doesMoveBlockCheck(newMove)) {
                        
                        return false
                    }
                }
            }
            if (this.boardState[p].teamType === this.colour && this.boardState[p].pieceType === 10) {
                validKingMoves = this.getValidKingMoves(this.boardState[p].x, this.boardState[p].y)
                for (let i = 0; i < validKingMoves.length; i++) {
                    newMove = {currentX: this.boardState[p].x, currentY: this.boardState[p].y, targetX:  validKingMoves[i].x, targetY: validKingMoves[i].y}
                    if (!this.doesMoveRevealCheck(newMove)) {
                        
                        return false
                    }
                }
            }
        }
        
        return true
            

    }

    isCheckmate() {
        let colourSwitch = new Map([['white', 'black'], ['black', 'white']])
        
        this.colour = colourSwitch.get(this.colour)
        if (this.isCheck(this.boardState)) {
            if (this.isStalemate()) {
                return true
            }
        }
        return false
    }

    switchTurn() {
        this.turn = this.switch.get(this.turn)
    }
    
}

module.exports = Board
