const randPiece = () => {
    return Math.random() > 0.5 ? 'white':'black'
}

module.exports = {randPiece};